var numSelected = null;
var tileSelected = null;

var errors = 0;
var points = 0;
var gameOver = false;


var board = [
    "--74916-5",
    "2---6-3-9",
    "-----7-1-",
    "-586----4",
    "--3----9-",
    "--62--187",
    "9-4-7---2",
    "67-83----",
    "81--45---",
];

var solution = [
    "387491625",
    "241568379",
    "569327418",
    "758619234",
    "123784596",
    "496253187",
    "934176852",
    "675832941",
    "812945763",
];

window.onload = function () {
    setGame();
};

function setGame() {
    // Digits 1-9
    for (let i = 1; i <= 9; i++) {
        //<div id="1" class="number">1</div>
        let number = document.createElement("div");
        number.id = i;
        number.innerText = i;
        number.addEventListener("click", selectNumber);
        number.classList.add("number");
        document.getElementById("digits").appendChild(number);
        document.getElementById("level").innerText = level;

        document.getElementById("gameOverModal").style.display = "none";

    }

    // Board 9x9
    for (let r = 0; r < 9; r++) {
        for (let c = 0; c < 9; c++) {
            let tile = document.createElement("div");
            tile.id = r.toString() + "-" + c.toString();
            if (board[r][c] != "-") {
                tile.innerText = board[r][c];
                tile.classList.add("tile-start");
            }
            if (r == 2 || r == 5) {
                tile.classList.add("horizontal-line");
            }
            if (c == 2 || c == 5) {
                tile.classList.add("vertical-line");
            }
            tile.addEventListener("click", selectTile);
            tile.classList.add("tile");
            document.getElementById("board").append(tile);
        }
    }
}

function selectNumber() {
    if (numSelected != null) {
        numSelected.classList.remove("number-selected");
    }
    numSelected = this;
    numSelected.classList.add("number-selected");
}

function selectTile() {
    // if(gameOver)
    // {
    //     return
    // }

    if (numSelected) {
        if (this.innerText != "") {
            return;
        }

        // "0-0" "0-1" .. "3-1"
        let coords = this.id.split("-"); //["0", "0"]
        let r = parseInt(coords[0]);
        let c = parseInt(coords[1]);

        if (solution[r][c] == numSelected.id) {
            this.innerText = numSelected.id;
            points += 1;
            console.log(points)
            document.getElementById("score").innerText = points;
            if (points % 5 === 0) increaseLevel();
        } else {
            errors += 1;
            document.getElementById("errors").innerText = errors;
            if(errors>5)
            {
                gameOver = true
                showGameOver();

            }
        }


    }
}

async function increaseLevel() {

    // let url = `{{ route('user.game.play', ['game' => ':game']) }}`;
    // url = url.replace(':game', game);
    let response = await fetch(playRoute, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        },
    });
    if (response.status === 200) {
        let data = await response.json();
        console.log("Level updated:", data.level);

            document.getElementById('level').innerText = data.level

        console.log("success");
    }
}


async function showGameOver() {

    let response = await fetch(userPointsRoute, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute("content"),
        },
    });

    if (response.status === 200) {
        let data = await response.json();
        document.getElementById("user-points").innerText = data.points;
    }


document.getElementById("gameOverModal").style.display = "block";
}

function reloadGame() {
    location.reload();
}
