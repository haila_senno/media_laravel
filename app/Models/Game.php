<?php

namespace App\Models;

use App\Enums\GameTypeEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Game extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;



    protected $casts = [];

    protected $guarded = [];


    ########## Relations ##########

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'pays')
        ->withPivot(['price','id'])
        ->withTimestamps();
    }

    public function scopeFree($query)
    {
        return $query->where('game_type',GameTypeEnum::FREE->value);
    }
    public function scopeNotFree($query)
    {
        return $query->where('game_type',GameTypeEnum::NOT_FREE->value);
    }

    ########## Libraries ##########


    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('Game1')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();
            $this
            ->addMediaCollection('Game2')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();
            $this
            ->addMediaCollection('Game3')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();

            $this
            ->addMediaCollection('GameSetUp')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();

    }

}
