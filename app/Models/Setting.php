<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Spatie\MediaLibrary\HasMedia;
//use Spatie\MediaLibrary\InteractsWithMedia;

class Setting extends Model //implements HasMedia
{
    use HasFactory;
    //use InteractsWithMedia;



    protected $casts = [];

    protected $guarded = [];

    public static function getPoints($price)
    {
        $setting = Setting::first();
        $points = 0;
            $mul = floor($price / $setting->price);
            $points +=  $setting->points * $mul;
        
        return $points;
    }


    ########## Relations ##########



    ########## Libraries ##########


    // public function registerMediaCollections(): void
    // {
    //     $this
    //         ->addMediaCollection('Setting')
    //         ->useFallbackUrl(config('app.url') . '/images/default.jpg')
    //         ->singleFile();
    // }

}
