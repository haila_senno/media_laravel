<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles,InteractsWithMedia;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $attributes = [ "points" => 0];

    /**
     * Get the rate that owns the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rates(): HasMany
    {
        return $this->hasMany(Rate::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function games(): BelongsToMany
    {
        return $this->belongsToMany(Game::class,'pays')
        ->withPivot(['price','id'])
        ->withTimestamps();
    }

    public function shows(): BelongsToMany
    {
        return $this->belongsToMany(Show::class,'buys')
        ->withPivot('price')
        ->withTimestamps();
    }

public function playGames(): BelongsToMany
{
    return $this->belongsToMany(Game::class,'plays')
    ->withPivot('level')
    ->withTimestamps();
}



    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('Admin')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();

        $this
            ->addMediaCollection('User')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();

    }
}
