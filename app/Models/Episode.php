<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Episode extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;



    protected $casts = [];

    protected $guarded = [];


    ########## Relations ##########

    /**
     * Get the show that owns the Episode
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function show(): BelongsTo
    {
        return $this->belongsTo(Show::class);
    }


    ########## Libraries ##########


    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('Episode')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();
    }
}
