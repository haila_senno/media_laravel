<?php

namespace App\Models;

use App\Enums\ShowTypeEnum;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Maize\Markable\Markable;
use Maize\Markable\Models\Like;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\Support\MediaStream;


class Show extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    // use Markable;


    // protected static $marks = [
    //     Like::class,
    // ];



    protected $casts = [];

    protected $guarded = [];

    protected $attributes = [
        "view_count" => 0,
    ];

    ########## Relations ##########


    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function teamMembers(): BelongsToMany
    {
        return $this->belongsToMany(TeamMember::class);
    }

    public function rates(): HasMany
    {
        return $this->hasMany(Rate::class);
    }

    public function comments(): hasMany
    {
        return $this->hasMany(Comment::class);
    }
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'buys')
        ->withPivot('price')
        ->withTimestamps();
    }
    public function processMedia()
    {
        $media = collect();
        foreach ($this->episodes as $episode) {
            $media = $media->merge($episode->getMedia('Episode'));
        }

        return MediaStream::create('episodes.zip')->addMedia($media);
    }


    public function episodes(): HasMany
    {
        return $this->hasMany(Episode::class);
    }

    public function getAverageRatingAttribute()
    {
        return $this->rates()->avg('value');
    }

    // protected function likeCount(): Attribute
    // {
    //     return Attribute::make(
    //         get: fn() => Like::count($this)
    //     );
    // }





    ########## Libraries ##########

    public function scopeMovie($query)
    {
        return $query->where('show_type', ShowTypeEnum::MOVIE->value);
    }

    public function scopeSeries($query)
    {
        return $query->where('show_type', ShowTypeEnum::SERIES->value);
    }

    public function scopeMinRate($query, $minRate)
    {
        return $query->whereHas('rates', function ($q) use ($minRate) {
            $q->selectRaw('AVG(value)')->havingRaw('AVG(value) >= ?', [$minRate]);
        });
    }


    public function scopeMaxRate($query, $maxRate)
    {
        return $query->whereHas('rates', function ($q) use ($maxRate) {
            $q->selectRaw('AVG(value)')->havingRaw('AVG(value) <= ?', [$maxRate]);
        });
    }

 public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('ShowImage')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();

        $this
            ->addMediaCollection('ShowPromo')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();

        $this
            ->addMediaCollection('ShowFile')
            ->useFallbackUrl(config('app.url') . '/images/default.jpg')
            ->singleFile();
    }


}
