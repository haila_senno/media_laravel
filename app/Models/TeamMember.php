<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

//use Spatie\MediaLibrary\HasMedia;
//use Spatie\MediaLibrary\InteractsWithMedia;

class TeamMember extends Model //implements HasMedia
{
    use HasFactory;
    //use InteractsWithMedia;



    protected $casts = [];

    protected $guarded = [];


    ########## Relations ##########

    public function shows(): BelongsToMany
    {
        return $this->belongsToMany(Show::class);
    }

    ########## Libraries ##########


    // public function registerMediaCollections(): void
    // {
    //     $this
    //         ->addMediaCollection('TeamMember')
    //         ->useFallbackUrl(config('app.url') . '/images/default.jpg')
    //         ->singleFile();
    // }

}
