<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Casts\Attribute;

use Maize\Markable\Markable;
use Maize\Markable\Models\Like;

//use Spatie\MediaLibrary\HasMedia;
//use Spatie\MediaLibrary\InteractsWithMedia;

class Comment extends Model //implements HasMedia
{
    use HasFactory;
    //use InteractsWithMedia;
    use Markable;



    protected $casts = [];

    protected $guarded = [];

    protected static $marks = [
        Like::class,
    ];


    ########## Relations ##########

    /**
     * Get the user that owns the Comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function show(): BelongsTo
    {
        return $this->belongsTo(Show::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comment(): BelongsTo
    {
        return $this->belongsTo(Comment::class,'parent_id');
    }


    public function replies(): HasMany
    {
        return $this->hasMany(Comment::class,'parent_id');
    }

    protected function likeCount(): Attribute
    {
        return Attribute::make(
            get: fn() => Like::count($this)
        );
    }
    ########## Libraries ##########


    // public function registerMediaCollections(): void
    // {
    //     $this
    //         ->addMediaCollection('Comment')
    //         ->useFallbackUrl(config('app.url') . '/images/default.jpg')
    //         ->singleFile();
    // }



}
