<?php


namespace App\Actions;

use App\Enums\RoleEnum;
use App\Models\Category;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Auth;

class GetCategoriesDependsOnBirth
{
    public static function handle()
    {

        $user = Auth::user();
        if($user)
        {
            $userAge = Carbon::parse($user->birth_date)->age;
            return $userAge >= 18 || $user->hasRole(RoleEnum::ADMIN->value) ? Category::all() : Category::whereNotIn('category_type', ['Romance', 'Horror'])->get();
        }
        return Category::all();
    }
}
