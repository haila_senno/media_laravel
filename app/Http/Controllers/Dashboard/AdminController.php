<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\RoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Admin\StoreAdminRequest;
use App\Http\Requests\Dashboard\Admin\UpdateAdminRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class AdminController extends Controller
{
    public function index(): View
    {
        $admins = QueryBuilder::for(User::role(RoleEnum::ADMIN->value))
            ->allowedFilters('username')
            ->latest('id')
            ->get();

        return view(
            'dashboard.pages.admin.index',
            compact('admins')
        );
    }

    public function create(): View
    {
        return view(
            'dashboard.pages.admin.create'
        );
    }


    public function store(StoreAdminRequest $request): RedirectResponse
    {
        // Store Admin
        $admin = User::create($request->validated())
            ->assignRole(RoleEnum::ADMIN->value);

        $admin->addMediaFromRequest('image')
            ->toMediaCollection('Admin');


        return redirect()->route('admin.index');
    }




    public function edit(User $admin): View
    {
        return view(
            'dashboard.pages.admin.edit',
            compact('admin')
        );
    }


    public function update(UpdateAdminRequest $request, User $admin): RedirectResponse
    {
        // Update Admin
        $admin->update($request->validated());

        $request->hasFile('image') &&
            $admin->addMediaFromRequest('image')
            ->toMediaCollection('Admin');

        return redirect()->route('admin.index');
    }


    public function destroy(User $admin): RedirectResponse
    {
        // Delete Admin
        $admin->delete();

        return back();
    }
}
