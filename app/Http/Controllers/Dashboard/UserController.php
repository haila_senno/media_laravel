<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\RoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\User\StoreUserRequest;
use App\Http\Requests\Dashboard\User\UpdateUserRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class UserController extends Controller
{
    public function index(): View
    {
        $users = QueryBuilder::for(User::role(RoleEnum::USER->value))
            ->allowedFilters('username')
            ->latest('id')
            ->get();

        return view(
            'dashboard.pages.user.index',
            compact('users')
        );
    }


    public function create(): View
    {

        return view(
            'dashboard.pages.user.create'
        );
    }


    public function store(StoreUserRequest $request): RedirectResponse
    {

        // Store User
        $user = User::create($request->validated())
            ->assignRole(RoleEnum::USER->value);

        $user->addMediaFromRequest('image')
            ->toMediaCollection('User');


        return redirect()->route('user.index');
    }




    public function edit(User $user): View
    {
        return view(
            'dashboard.pages.user.edit',
            compact('user')
        );
    }


    public function update(UpdateUserRequest $request, User $user): RedirectResponse
    {
        // Update Admin
        $user->update($request->validated());

        $request->hasFile('image') &&
            $user->addMediaFromRequest('image')
            ->toMediaCollection('User');

        return redirect()->route('user.index');
    }


    public function destroy(User $user): RedirectResponse
    {
        // Delete Admin
        $user->delete();

        return back();
    }
}
