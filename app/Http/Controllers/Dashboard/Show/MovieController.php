<?php

namespace App\Http\Controllers\Dashboard\Show;

use App\Enums\ShowTypeEnum;
use App\Enums\ShowYearEnum;
use App\Http\Controllers\Controller;
use App\Models\Show;
use App\Http\Requests\Dashboard\Movie\StoreMovieRequest;
use App\Http\Requests\Dashboard\Movie\UpdateMovieRequest;
use App\Models\Category;
use App\Models\TeamMember;
use App\Models\Setting;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Request;
use Illuminate\View\View;
use Spatie\QueryBuilder\QueryBuilder;

class MovieController extends Controller
{

    public function index(Request $request): View
    {
        $years = ShowYearEnum::getTypesForSelect();
        $movies = QueryBuilder::for(Show::movie())
        ->with(['category', 'teamMembers'])
        ->allowedFilters('show_name')
        ->latest('id')
        ->get();

        return view(
            'dashboard.pages.movie.index',
            compact('movies')
        );
    }


    public function create(): View
    {
        $years = ShowYearEnum::getTypesForSelect();

        $teamMembers = TeamMember::all();
        $categories = Category::all();
        $show_types = ShowTypeEnum::getTypesForSelect();

        return view(
            'dashboard.pages.movie.create',
            compact(['categories', 'show_types', 'teamMembers', 'years'])
        );
    }


    public function store(StoreMovieRequest $request): RedirectResponse
    {
        // Store Show
        $points = Setting::getPoints($request->price);
        $show = Show::create([
           ...$request->validated(),
           'points' => $points
        ]);
        $show->teamMembers()->sync($request->team_members);

        // Add media to Show
        $show
            ->addMediaFromRequest('image')
            ->toMediaCollection('ShowImage');

        $show
            ->addMediaFromRequest('promo')
            ->toMediaCollection('ShowPromo');

        $show
            ->addMediaFromRequest('file')
            ->toMediaCollection('ShowFile');

        return redirect()->route('movie.index');
    }


    public function edit(Show $show): View
    {
        $years = ShowYearEnum::getTypesForSelect();
        $teamMembers = TeamMember::all();
        $categories = Category::all();
        $show_types = ShowTypeEnum::getTypesForSelect();

        return view(
            'dashboard.pages.movie.edit',
            compact(['show', 'categories', 'show_types', 'teamMembers', 'years'])
        );
    }


    public function update(UpdateMovieRequest $request, Show $show): RedirectResponse
    {
        $points = $show->price != $request->price ?
        Setting::getPoints($request->price) : $show->points;
        // Update Show
        $show->update([
            ...$request->validated(),
            'points' => $points
        ]);
        $show->teamMembers()->sync($request->team_members);
        // Edit Image for  Show if exist
        $request->hasFile('image') &&
            $show
            ->addMediaFromRequest('image')
            ->toMediaCollection('ShowImage');

        $request->hasFile('file') &&
            $show
            ->addMediaFromRequest('file')
            ->toMediaCollection('ShowFile');

        $request->hasFile('promo') &&
            $show
            ->addMediaFromRequest('promo')
            ->toMediaCollection('ShowPromo');

        // dd($show->getFirstMediaUrl('ShowImage'));


        return redirect()->route('movie.index');
    }


    public function destroy(Show $show): RedirectResponse
    {
        // Delete Show
        $show->delete();

        return back();
    }
}
