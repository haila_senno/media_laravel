<?php

namespace App\Http\Controllers\Dashboard\Show;

use App\Enums\ShowTypeEnum;
use App\Enums\ShowYearEnum;
use App\Http\Controllers\Controller;
use App\Models\Show;
use App\Http\Requests\Dashboard\Series\StoreSeriesRequest;
use App\Http\Requests\Dashboard\Series\UpdateSeriesRequest;
use App\Models\Category;
use App\Models\Setting;
use App\Models\TeamMember;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\QueryBuilder\QueryBuilder;

class SeriesController extends Controller
{

    public function index(): View
    {
        $serieses = QueryBuilder::for(Show::series())
        ->with(['category', 'teamMembers'])
        ->allowedFilters('show_name')
        ->latest('id')
        ->get();
        return view(
            'dashboard.pages.series.index',
            compact('serieses')
        );
    }


    public function create(): View
    {
        $years = ShowYearEnum::getTypesForSelect();

        $teamMembers = TeamMember::all();
        $categories = Category::all();
        $show_types = ShowTypeEnum::getTypesForSelect();

        return view(
            'dashboard.pages.series.create',
            compact(['categories', 'show_types', 'teamMembers', 'years'])
        );
    }


    public function store(StoreSeriesRequest $request): RedirectResponse
    {
        // Store Show
        $points = Setting::getPoints($request->price);
        $show = Show::create([
            ...$request->validated(),
            'points' => $points
        ]);
        $show->teamMembers()->sync($request->team_members);


        // Add media to Show
        $show
            ->addMediaFromRequest('image')
            ->toMediaCollection('ShowImage');

        $show
            ->addMediaFromRequest('promo')
            ->toMediaCollection('ShowPromo');


        return redirect()->route('series.index');
    }


    public function edit(Show $show): View
    {
        $years = ShowYearEnum::getTypesForSelect();

        $categories = Category::all();
        $teamMembers = TeamMember::all();

        $show_types = ShowTypeEnum::getTypesForSelect();

        return view(
            'dashboard.pages.series.edit',
            compact(['show', 'categories', 'show_types', 'teamMembers', 'years'])
        );
    }


    public function update(UpdateSeriesRequest $request, Show $show): RedirectResponse
    {
        $points = $show->price != $request->price ?
        Setting::getPoints($request->price) : $show->points;
        // Update Show
        $show->update([
            ...$request->validated(),
            'points' => $points
        ]);

        $show->teamMembers()->sync($request->team_members);
        // Edit Image for  Show if exist
        $request->hasFile('image') &&
            $show
            ->addMediaFromRequest('image')
            ->toMediaCollection('ShowImage');

        $request->hasFile('file') &&
            $show
            ->addMediaFromRequest('file')
            ->toMediaCollection('ShowFile');

        $request->hasFile('promo') &&
            $show
            ->addMediaFromRequest('promo')
            ->toMediaCollection('ShowPromo');

            return redirect()->route('series.index');
        }


    public function destroy(Show $show): RedirectResponse
    {
        // Delete Show
        $show->delete();

        return back();
    }
}
