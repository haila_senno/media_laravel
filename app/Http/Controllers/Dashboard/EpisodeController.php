<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Episode\StoreEpisodeRequest;
use App\Http\Requests\Dashboard\Episode\UpdateEpisodeRequest;
use App\Models\Episode;
use App\Models\Show;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\QueryBuilder\QueryBuilder;

class EpisodeController extends Controller
{

    public function index(): View
    {
        $episodes = QueryBuilder::for(Episode::class)
        ->with('show')
        ->allowedFilters('episode_name')
        ->latest('id')
        ->get();


        return view(
            'dashboard.pages.episode.index',
            compact('episodes')
        );
    }


    public function create(): View
    {
        $shows = Show::series()->get();
        return view(
            'dashboard.pages.episode.create',compact('shows'),
        );
    }


    public function store(StoreEpisodeRequest $request): RedirectResponse
    {
        // Store Episode
        $episode = Episode::create($request->validated());


        // Add Image to Episode
        $episode
            ->addMediaFromRequest('file')
            ->toMediaCollection('Episode');

        return redirect()->route('episode.index');

    }


    public function edit(Episode $episode): View
    {
        $shows = Show::series()->get();

        return view(
            'dashboard.pages.episode.edit',
            compact(['episode','shows'])
        );
    }


    public function update(UpdateEpisodeRequest $request, Episode $episode): RedirectResponse
    {
        // Update Episode
        $episode->update($request->validated());


        // Edit Image for  Episode if exist
        $request->hasFile('file') &&
            $episode
                ->addMediaFromRequest('file')
                ->toMediaCollection('Episode');

                return redirect()->route('episode.index');
            }


    public function destroy(Episode $episode): RedirectResponse
    {
        // Delete Episode
        $episode->delete();

        return back();
    }
}
