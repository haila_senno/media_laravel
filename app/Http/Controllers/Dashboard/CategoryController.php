<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Category\StoreCategoryRequest;
use App\Http\Requests\Dashboard\Category\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryController extends Controller
{

    public function index(): View
    {
        $categories = QueryBuilder::for(Category::class)
        ->allowedFilters('category_type')
        ->latest('id')->get();

        return view(
            'dashboard.pages.category.index',
            compact('categories')
        );
    }


    public function create(): View
    {
        return view(
            'dashboard.pages.category.create',
        );
    }


    public function store(StoreCategoryRequest $request): RedirectResponse
    {
        // Store Category
        $category = Category::create($request->validated());

        return redirect()->route('category.index');

    }


    public function edit(Category $category): View
    {
        return view(
            'dashboard.pages.category.edit',
            compact('category')
        );
    }


    public function update(UpdateCategoryRequest $request, Category $category): RedirectResponse
    {
        // Update Category
        $category->update($request->validated());

        return redirect()->route('category.index');
    }


    public function destroy(Category $category): RedirectResponse
    {
        // Delete Category
        $category->delete();

        return back();
    }
}
