<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use App\Notifications\AcceptBuyPointsNotification;
use App\Notifications\RejectBuyPointsNotification;
use Illuminate\Http\Request;

class PayController extends Controller
{

    public function index(User $user, Setting $setting)
    {
        return view('dashboard.pages.pay.index',compact(['user','setting']));
    }

    public function accept(User $user, Setting $setting)
    {
        $user->increment('points' , $setting->points);

        $user->notify(new AcceptBuyPointsNotification($setting,$user));
        return back();


    }

    public function reject(User $user, Setting $setting)
    {

        $user->notify(new RejectBuyPointsNotification($setting,$user));
        return back();


    }
}
