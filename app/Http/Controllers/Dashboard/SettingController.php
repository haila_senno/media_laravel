<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Setting\StoreSettingRequest;
use App\Http\Requests\Dashboard\Setting\UpdateSettingRequest;
use App\Models\Setting;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index() {

        $settings = Setting::latest('id')->get();

        return view('dashboard.pages.setting.index',compact('settings'));

    }

    public function create() {


        return view('dashboard.pages.setting.create');

    }

    public function store(StoreSettingRequest $request): RedirectResponse
    {
        // Store TeamMember
        Setting::create($request->validated());

        return redirect()->route('setting.index');

    }

    public function edit(Setting $setting) {


        return view('dashboard.pages.setting.edit',compact('setting'));

    }

    public function update(UpdateSettingRequest $request,Setting $setting): RedirectResponse
    {
        // Store TeamMember
        $setting->update($request->validated());

        return redirect()->route('setting.index');

    }

    public function destroy(Setting $setting): RedirectResponse
    {
        // Delete Admin
        $setting->delete();

        return back();
    }
}
