<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\RoleEnum;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Show;
use App\Models\Game;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {

        $startMonth = now()->startOfMonth();
        $today = now()->endOfDay();
        $users = User::role(RoleEnum::USER->value)
            // ->whereBetween('created_at', [$startMonth, $today])
            ->count();
        $movies = Show::movie()
            // ->whereBetween('created_at', [$startMonth, $today])
            ->count();
        $series = Show::series()
            //  ->whereBetween('created_at', [$startMonth, $today])
            ->count();
        $games = Game::
            //  whereBetween('created_at', [$startMonth, $today])
            count();

        $paidGames = Game::withCount('users')
            ->having('users_count', '!=', 0)
            ->take(10)
            ->orderBy('users_count', 'desc')
            ->get();


        $shows = Show::selectRaw('YEAR(created_at) as year, MONTH(created_at) as month, COUNT(*) as show_count')
            ->groupBy('year', 'month')
            ->orderBy('show_count', 'desc')
            ->get();
        $showsViews = Show::selectRaw('YEAR(created_at) as year, MONTH(created_at) as month, SUM(view_count) as view_count')
            ->groupBy('year', 'month')
            ->orderBy('view_count', 'desc')
            ->get();

        $showsRates = Show::selectRaw('YEAR(shows.created_at) as year, MONTH(shows.created_at) as month, Avg(rates.value) as total_rate')
            ->join('rates', 'shows.id', '=', 'rates.show_id')
            ->groupBy('year', 'month')
            ->orderBy('total_rate', 'desc')
            ->get();

        $payedShows = Show::selectRaw('YEAR(shows.created_at) as year, MONTH(shows.created_at) as month, Count(*) as show_count')
            ->join('buys', 'shows.id', '=', 'buys.show_id')
            ->groupBy('year', 'month')
            ->orderBy('show_count', 'desc')
            ->get();
        // Prepare the data for the chart
        $dates = [];
        $showCounts = [];
        $viewCounts = [];
        $rateCounts = [];
        $payedCount = [];

        foreach ($shows as $show) {
            $dates[] = Carbon::create($show->year, $show->month)->format('Y-m');
            $showCounts[] = $show->show_count;
        }

        foreach ($showsViews as $show) {
            $viewCounts[] = $show->view_count;
        }

        foreach ($showsRates as $show) {
            $rateCounts[] = $show->total_rate;
        }

        foreach ($payedShows as $show) {
            $payedCount[] = $show->show_count;
        }


        $chartData = [
            'labels' => $dates,
            'shows' => $showCounts,
            'views' => $viewCounts,
            'rates' => $rateCounts,
            'payed' => $payedCount,
        ];
        return view('dashboard.pages.dashboard', compact(['users', 'movies', 'series', 'games', 'paidGames', 'chartData']));
    }
}
