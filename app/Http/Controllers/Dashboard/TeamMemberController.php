<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\TeamMemberTypeEnum;
use App\Http\Controllers\Controller;
use App\Models\TeamMember;
use App\Http\Requests\Dashboard\TeamMember\StoreTeamMemberRequest;
use App\Http\Requests\Dashboard\TeamMember\UpdateTeamMemberRequest;
use App\Models\Show;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\QueryBuilder\QueryBuilder;

class TeamMemberController extends Controller
{

    public function index(): View
    {
        $teamMembers = QueryBuilder::for(TeamMember::class)
        ->allowedFilters('team_member_name')
        ->latest('id')
        ->get();

        return view(
            'dashboard.pages.teamMember.index',
            compact('teamMembers')
        );
    }


    public function create(): View
    {
        $shows = Show::all();
        $teamMemberTypes = TeamMemberTypeEnum::getTypesForSelect();

        return view(
            'dashboard.pages.teamMember.create',compact(['shows','teamMemberTypes']),
        );
    }


    public function store(StoreTeamMemberRequest $request): RedirectResponse
    {
        // Store TeamMember
        $teamMember = TeamMember::create($request->validated());


        return redirect()->route('teamMember.index');

    }




    public function edit(TeamMember $teamMember): View
    {
        $teamMemberTypes = TeamMemberTypeEnum::getTypesForSelect();
        $shows = Show::all();
        return view(
            'dashboard.pages.teamMember.edit',
            compact(['teamMember','shows','teamMemberTypes'])
        );
    }


    public function update(UpdateTeamMemberRequest $request, TeamMember $teamMember): RedirectResponse
    {
        // Update TeamMember
        $teamMember->update($request->validated());

        return redirect()->route('teamMember.index');
    }


    public function destroy(TeamMember $teamMember): RedirectResponse
    {
        // Delete TeamMember
        $teamMember->delete();

        return back();
    }
}
