<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\GameTypeEnum;
use App\Enums\ShowYearEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Game\StoreGameRequest;
use App\Http\Requests\Dashboard\Game\UpdateGameRequest;
use App\Models\Game;
use App\Models\Setting;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\QueryBuilder\QueryBuilder;

class GameController extends Controller
{

    public function index(): View
    {
        $games = QueryBuilder::for(Game::class)
        ->allowedFilters('game_name')
        ->latest('id')
        ->get();


        return view(
            'dashboard.pages.game.index',
            compact('games')
        );
    }


    public function create(): View
    {
        $game_types = GameTypeEnum::getTypesForSelect();
        $years = ShowYearEnum::getTypesForSelect();

        return view(
            'dashboard.pages.game.create',
            compact(['game_types', 'years'])
        );
    }


    public function store(StoreGameRequest $request): RedirectResponse
    {
        // Store Game
        $points = $request->game_type == GameTypeEnum::NOT_FREE->value ?
            Setting::getPoints($request->price) : 0;
        $game = Game::create([
            ...$request->validated(),
            'points' => $points
        ]);

        // Add Image to Game
        $request->hasFile('setup') &&
            $game->addMediaFromRequest('setup')
            ->toMediaCollection('GameSetUp');

        $game
            ->addMediaFromRequest('image1')
            ->toMediaCollection('Game1');

        $request->hasFile('image2') &&
            $game->addMediaFromRequest('image2')
            ->toMediaCollection('Game2');

        $request->hasFile('image3') &&
            $game->addMediaFromRequest('image3')
            ->toMediaCollection('Game3');




        return redirect()->route('game.index');
    }




    public function edit(Game $game): View
    {
        $game_types = GameTypeEnum::getTypesForSelect();
        $years = ShowYearEnum::getTypesForSelect();

        return view(
            'dashboard.pages.game.edit',
            compact(['game_types', 'game', 'years'])
        );
    }


    public function update(UpdateGameRequest $request, Game $game): RedirectResponse
    {
        // Update Game
        $points = $request->price && GameTypeEnum::NOT_FREE->value ?
            Setting::getPoints($request->price) : 0;
        $game->update([
            ...$request->validated(),
            'points' => $points
        ]);


        // Edit Image for  Game if exist
        $request->hasFile('setup') &&
            $game
            ->addMediaFromRequest('setup')
            ->toMediaCollection('GameSetUp');
        $request->hasFile('image1') &&
            $game
            ->addMediaFromRequest('image1')
            ->toMediaCollection('Game1');
        $request->hasFile('image2') &&
            $game
            ->addMediaFromRequest('image2')
            ->toMediaCollection('Game2');
        $request->hasFile('image3') &&
            $game
            ->addMediaFromRequest('image3')
            ->toMediaCollection('Game3');

        return redirect()->route('game.index');
    }


    public function destroy(Game $game): RedirectResponse
    {
        // Delete Game
        $game->delete();

        return back();
    }
}
