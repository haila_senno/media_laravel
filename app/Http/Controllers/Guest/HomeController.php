<?php

namespace App\Http\Controllers\Guest;

use App\Actions\GetCategoriesDependsOnBirth;
use App\Enums\ShowYearEnum;
use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;

class HomeController extends Controller
{
    public function index()
    {
        $categories = (new GetCategoriesDependsOnBirth)->handle();
        $latestMovies = Show::movie()
            ->whereHas('category', function ($q) use ($categories) {
                $q->whereIn('id', $categories->pluck('id')->toArray());
            })
            ->where('release_date', ShowYearEnum::YEAR_2024->value)
            ->with(['category', 'rates'])
            ->take(8)
            ->latest('id')
            ->get();

        $mostWatched = Show::series()
            ->whereHas('category', function ($q) use ($categories) {
                $q->whereIn('id', $categories->pluck('id')->toArray());
            })
            ->where('view_count', '>', 0)
            ->orderByDesc('view_count')
            ->with(['category', 'rates'])
            ->take(6)
            ->get();

        $highestRateMovies = Show::movie()
            ->whereHas('category', function ($q) use ($categories) {
                $q->whereIn('id', $categories->pluck('id')->toArray());
            })
            ->has('rates')
            ->with(['category', 'rates'])
            ->orderByDesc(DB::raw('(SELECT AVG(value) FROM rates WHERE show_id = shows.id)'))
            ->take(10)
            ->get();

        $highestRateSeries = Show::series()
            ->whereHas('category', function ($q) use ($categories) {
                $q->whereIn('id', $categories->pluck('id')->toArray());
            })
            ->has('rates')
            ->with(['category', 'rates'])
            ->orderByDesc(DB::raw('(SELECT AVG(value) FROM rates WHERE show_id = shows.id)'))
            ->take(10)
            ->get();

        return view('guest.pages.index', compact(['latestMovies', 'mostWatched', 'highestRateMovies', 'highestRateSeries']));
    }

    public function about()
    {
        return view('guest.pages.about');
    }
    public function help()
    {
        return view('guest.pages.help');
    }

    // public function search()
    // {
    //     $show = QueryBuilder::for(Show::class)
    //                 ->allowedFilters('show_name')
    //                 ->first();
    //     return view('guest.pages.movie.show',compact('show'));
    // }
}
