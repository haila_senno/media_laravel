<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Http\Requests\Guest\Comment\StoreCommentRequest;
use App\Http\Requests\Guest\Comment\UpdateCommentRequest;
use App\Models\Comment;
use App\Models\Show;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CommentController extends Controller
{


    public function store(StoreCommentRequest $request,Show $show)
    {
        // Store Comment
        // dd($request->validated());
        $comment = $show->comments()->create($request->validated());
        return back();

    }




    public function show(Comment $comment): View
    {
        return view(
            'folder.pages.$comment.show',
            compact('$comment')
        );
    }


    public function edit(Comment $comment): View
    {
        return view(
            'folder.pages.$comment.edit',
            compact('$comment')
        );
    }


    public function update(UpdateCommentRequest $request, Comment $comment): RedirectResponse
    {
        // Update Comment
        $comment->update($request->validated());


        // Edit Image for  Comment if exist
        $request->hasFile('image') &&
            $comment
                ->addMediaFromRequest('image')
                ->toMediaCollection('Comment');

        return back();
    }


    public function destroy(Comment $comment): RedirectResponse
    {
        // Delete Comment
        $comment->delete();

        return back();
    }
}
