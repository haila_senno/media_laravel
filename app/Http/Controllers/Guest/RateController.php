<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Http\Requests\Guest\Rate\StoreRateRequest;
use App\Http\Requests\Guest\Rate\UpdateRateRequest;
use App\Models\Rate;
use App\Models\Show;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class RateController extends Controller
{


    public function store(StoreRateRequest $request, Show $show): RedirectResponse
    {
        // Store Rate
        $user = Auth::user();
        $rate = $user->rates()->where('show_id', $show->id)->first();
        if ($rate)
            $rate->update(['value' => $request->validated()['value']]);
        else
            $rate = $show->rates()->create([
                ...$request->validated(),
                'user_id' => $user->id
            ]);

        return back();

    }
}
