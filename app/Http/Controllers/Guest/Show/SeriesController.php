<?php

namespace App\Http\Controllers\Guest\Show;

use App\Actions\GetCategoriesDependsOnBirth;
use App\Enums\ShowTypeEnum;
use App\Enums\ShowYearEnum;
use App\Http\Controllers\Controller;
use App\Models\Show;
use App\Http\Requests\Dashboard\Series\StoreSeriesRequest;
use App\Http\Requests\Dashboard\Series\UpdateSeriesRequest;
use App\Models\Category;
use App\Models\TeamMember;
use App\Services\ShowService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Spatie\MediaLibrary\Support\MediaStream;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SeriesController extends Controller
{

    public function index(Request $request): View
    {
        // dd($request->all());
        $categories = (new GetCategoriesDependsOnBirth)->handle();
        $serieses = QueryBuilder::for(Show::series())
            ->whereHas('category', function ($q) use ($categories) {
                $q->whereIn('id', $categories->pluck('id')->toArray());
            })
            ->with(['category', 'rates'])
            ->allowedFilters([
                AllowedFilter::exact('category_id'),
                AllowedFilter::exact('release_date'),
                AllowedFilter::scope('min_rate')->ignore(0),
                AllowedFilter::scope('max_rate')->ignore(10),
                'show_name'
            ])
            ->latest('id')
            ->paginate(5)
            ->appends(request()->query());
        $years = ShowYearEnum::getTypesForSelect();

        return view('guest.pages.series.index', compact(['serieses', 'categories', 'years']));
    }


    public function show(Show $show): View
    {
        $show->increment('view_count');


        $comments = $show->comments()
            ->where('parent_id', null)
            ->with('replies.user')
            ->latest('id')
            ->get();
        $rates = $show->rates()
            ->with('user')
            ->latest('id')
            ->get();

        return view(
            'guest.pages.series.show',
            compact(['show', 'comments', 'rates'])
        );
    }

    public function download(Show $show)
    {
        // (new ShowService)->attach($show,$user);
        //  return (new ShowService)->download($show,$user);

        $user = Auth::user();
        $user->shows()->syncWithoutDetaching([$show->id => ['price' => $show->price]]);
        $user->decrement('points', $show->points);

        $media = collect();
        foreach ($show->episodes as $episode) {
            $media = $media->merge($episode->getMedia('Episode'));
        }

        return MediaStream::create($show->show_name . '.zip')->addMedia($media);
    }
}
