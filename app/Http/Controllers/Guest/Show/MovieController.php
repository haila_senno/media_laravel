<?php

namespace App\Http\Controllers\Guest\Show;

use App\Actions\GetCategoriesDependsOnBirth;
use App\Enums\ShowTypeEnum;
use App\Enums\ShowYearEnum;
use App\Http\Controllers\Controller;
use App\Models\Show;
use App\Http\Requests\Dashboard\Movie\StoreMovieRequest;
use App\Http\Requests\Dashboard\Movie\UpdateMovieRequest;
use App\Models\Category;
use App\Models\TeamMember;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MovieController extends Controller
{

    public function index(Request $request): View
    {
        // dd($request->all());
        $categories = (new GetCategoriesDependsOnBirth)->handle();
         $movies = QueryBuilder::for(Show::movie())
            ->whereHas('category', function ($q) use ($categories) {
                $q->whereIn('id', $categories->pluck('id')->toArray());
            })
            ->with(['category', 'rates'])
            ->allowedFilters([
                AllowedFilter::exact('category_id'),
                AllowedFilter::exact('release_date'),
                AllowedFilter::scope('min_rate')->ignore(0),
                AllowedFilter::scope('max_rate')->ignore(10),
                'show_name'
            ])
            ->latest('id')
            ->paginate(5)
            ->appends(request()->query());

        $years = ShowYearEnum::getTypesForSelect();
        return view('guest.pages.movie.index', compact(['movies', 'categories', 'years']));
    }

    public function show(Show $show): View
    {
        $user = Auth::user();

        $show->increment('view_count');

        $comments = $show->comments()
            ->where('parent_id', null)
            ->with('replies.user')
            ->latest('id')
            ->get();

        $rates = $show->rates()
            ->with('user')
            ->latest('id')
            ->get();

        return view(
            'guest.pages.movie.show',
            compact(['show', 'comments', 'rates'])
        );
    }

    public function download(Show $show)
    {
        $user = Auth::user();

        $user->shows()->syncWithoutDetaching([$show->id => ['price' => $show->price]]);
        $user->decrement('points', $show->points);


        // $show->getFirstMedia('ShowFile');
        return $show->getFirstMedia('ShowFile');
    }
}

    // public function download(Show $show)
    // {
    //     // return view(
    //     //     'guest.pages.movie.show',
    //     //     compact('show')
    //     // );
    // }
