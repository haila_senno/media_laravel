<?php

namespace App\Http\Controllers\guest;

use App\Http\Controllers\Controller;
use App\Http\Requests\Guest\User\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function show()
    {
        $user = Auth::user();
        return view('guest.pages.user.show', compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        // dd($request->validated());
        $user->update($request->validated());

        $request->hasFile('image') &&
            $user->addMediaFromRequest('image')
            ->toMediaCollection('User');
            return redirect()->route('user.show',compact('user'));

    }
}
