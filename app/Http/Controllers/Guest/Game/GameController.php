<?php

namespace App\Http\Controllers\Guest\Game;

use App\Enums\GameTypeEnum;
use App\Enums\ShowYearEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Game\StoreGameRequest;
use App\Http\Requests\Dashboard\Game\UpdateGameRequest;
use App\Models\Game;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{

    public function index(Request $request): View
    {
        $games = QueryBuilder::for(Game::notFree())
        ->allowedFilters([
            AllowedFilter::exact('date'),
            'game_name'
        ])
        ->latest('id')
        ->paginate(5)
        ->appends(request()->query());


        $years = ShowYearEnum::getTypesForSelect();
        return view('guest.pages.game.index',compact(['games','years']));
    }


    public function show(Game $game): View
    {
        return view(
            'guest.pages.game.show',
            compact('game')
        );
    }


    public function download(Game $game)
    {
        $user = Auth::user();
        $user->games()->syncWithoutDetaching([$game->id => ['price' => $game->price]]);
        $user->decrement('points',$game->points);


        return $game->getFirstMedia('GameSetUp');
    }



}
