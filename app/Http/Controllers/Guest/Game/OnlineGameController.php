<?php

namespace App\Http\Controllers\Guest\Game;

use App\Enums\GameTypeEnum;
use App\Enums\OnlineGameEnum;
use App\Enums\ShowYearEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Game\StoreGameRequest;
use App\Http\Requests\Dashboard\Game\UpdateGameRequest;
use App\Models\Game;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OnlineGameController extends Controller
{

    public function index(): View
    {
        $games = QueryBuilder::for(Game::free())
            ->allowedFilters([
                AllowedFilter::exact('date'),
                'game_name'
            ])
            ->latest('id')
            ->paginate(3)
            ->appends(request()->query());


        $years = ShowYearEnum::getTypesForSelect();
        return view('guest.pages.game.online.index', compact(['games', 'years']));
    }


    public function show(Game $game): View
    {
       $user = Auth::user();
       $userPoints = $user->points;

        $level = $user->playGames()->where('game_id',$game->id)->exists()
         ? $user->playGames()->where('game_id',$game->id)->first()->pivot->level : 0;
        $view = '';
        $view = match ($game->id) {
            OnlineGameEnum::SNAKE->value => 'guest.pages.game.online.snake',
            OnlineGameEnum::CANDY->value => 'guest.pages.game.online.candy',
            OnlineGameEnum::SUDOKU->value => 'guest.pages.game.online.sudoku',
            default => abort(404),
        };
        return view(
            $view,
            compact(['game','level','userPoints'])
        );
    }

    public function play( Game $game)
    {

        $user = Auth::user();
        if ($user->playGames()->where('game_id', $game->id)->exists())
            $user->playGames()->updateExistingPivot($game->id, ['level' => DB::raw('level + 1')]);
        else
            $user->playGames()->syncWithoutDetaching([$game->id => ['level' => DB::raw('level + 1')]]);
        $user->update(['points' => DB::raw('points + 5')]);
        $level = $user->playGames()->where('game_id',$game->id)->first()->pivot->level;
        return response()->json([
            'level' => $level,
        ]);
    }

    public function getUserPoints() {
        $points = Auth::user()->points;
        return response()->json([
            'points' => $points
        ]);
    }
}
