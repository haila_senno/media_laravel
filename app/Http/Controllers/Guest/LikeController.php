<?php

namespace App\Http\Controllers\guest;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maize\Markable\Models\Like;

class LikeController extends Controller
{
    public function toggleLike(Comment $comment)
    {
        // dd($comment);
        $user = Auth::user();
        Like::toggle($comment, $user);
        return back();

    }
}
