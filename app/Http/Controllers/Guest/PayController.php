<?php

namespace App\Http\Controllers\Guest;

use App\Enums\RoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\guest\Pay\StorePayRequest;
use App\Models\Setting;
use App\Models\User;
use App\Notifications\BuyPointsNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class PayController extends Controller
{
    public function index()
    {
        $settings = Setting::latest('id')->get();
         return view('guest.pages.pay',compact('settings'));
    }

    public function buy(Setting $setting)
    {
        $user = Auth::user();

        // $user->increment('points' , $setting->points);
        $admins = User::role(RoleEnum::ADMIN->value)->get();
        Notification::send($admins,new BuyPointsNotification($setting,$user));

        // $admin->notify(new BuyPointsNotification($setting,$user));

        return back();

    }
}
