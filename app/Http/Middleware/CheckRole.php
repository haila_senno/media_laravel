<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next,$role): Response
    {


        if (!Auth::check()) {
            // If the user is not authenticated, redirect to login
            return redirect()->route('user.login');
        }


        if (Auth::check() && !$request->user()->hasRole($role)) {
            // If the user does not have the required role, abort with 403 Forbidden
            Auth::logout();
            abort(403, 'Unauthorized');
        }

        return $next($request);
    }
}
