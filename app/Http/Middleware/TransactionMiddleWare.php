<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\DB;


use Closure;
use Illuminate\Http\Request;
use Throwable;


class TransactionMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        DB::beginTransaction();

        $response =  $next($request);

        if (property_exists($response, 'exception'))
            $response->exception instanceof Throwable ? DB::rollBack() : DB::commit();

        return $response;
        return $next($request);
    }
}
