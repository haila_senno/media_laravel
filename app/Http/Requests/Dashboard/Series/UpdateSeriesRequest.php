<?php

namespace App\Http\Requests\Dashboard\Series;

use App\Enums\ShowTypeEnum;
use App\Enums\ShowYearEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateSeriesRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            "show_name" => ["nullable", "string"],
            "price" => ["nullable", "numeric"],
            "summery" => ["nullable", "string"],
            "release_date" => ["nullable", "integer",new Enum(ShowYearEnum::class)],
            "category_id" => ["nullable", "exists:categories,id"],
            "image" => ["nullable","mimes:jpg,bmp,png,webp"],
            "promo" => ["nullable","mimetypes:video/mp4"],
            "team_members" => ["array","min:1"],
            "team_members.*" => ["integer","exists:team_members,id"],

        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data =   [
            "show_name" => $this->show_name,
            "price" => $this->price,
            "summery" => $this->summery,
            "release_date" => $this->release_date,
            "category_id" => $this->category_id,
        ];
        return array_filter($data,fn($value) => !is_null($value));

    }

}
