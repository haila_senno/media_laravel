<?php

namespace App\Http\Requests\Dashboard\Series;

use App\Enums\ShowTypeEnum;
use App\Enums\ShowYearEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class StoreSeriesRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "show_name" => ["required", "string"],
            "price" => ["required", "numeric"],
            "summery" => ["required", "string"],
            "release_date" => ["required", "integer",new Enum(ShowYearEnum::class)],
            "category_id" => ["required", "exists:categories,id"],
            "image" => ["required","mimes:jpg,bmp,png,webp"],
            "promo" => ["required","mimetypes:video/mp4"],
            "team_members" => ["required","array","min:1"],
            "team_members.*" => ["integer","exists:team_members,id"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return [
            "show_name" => $this->show_name,
            "price" => $this->price,
            "summery" => $this->summery,
            "release_date" => $this->release_date,
            "category_id" => $this->category_id,
            "show_type" => ShowTypeEnum::SERIES->value,
        ];
    }
}
