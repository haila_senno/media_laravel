<?php

namespace App\Http\Requests\Dashboard\TeamMember;

use App\Enums\TeamMemberTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class StoreTeamMemberRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "team_member_name"=>["required","string","unique:team_members,team_member_name"],
            "team_member_type"=>["required",new Enum(TeamMemberTypeEnum::class)],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }

}
