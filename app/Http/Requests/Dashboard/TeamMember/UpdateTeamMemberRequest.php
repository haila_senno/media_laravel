<?php

namespace App\Http\Requests\Dashboard\TeamMember;

use App\Enums\TeamMemberTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateTeamMemberRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $teamMember = $this->route('teamMember');
        return [
            "team_member_name"=>["nullable","string",Rule::unique('team_members')->ignore($teamMember)],
            "team_member_type"=>["nullable",new Enum(TeamMemberTypeEnum::class)],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }

}
