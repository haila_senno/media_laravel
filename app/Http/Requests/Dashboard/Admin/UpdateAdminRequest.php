<?php

namespace App\Http\Requests\Dashboard\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateAdminRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'username' => ['nullable','string', Rule::unique('users')->ignore($this->route('admin'))],
            'email' => ['nullable', 'string', 'email', Rule::unique('users')->ignore($this->route('admin'))],
            'password' => ['nullable', 'string', 'confirmed'],
            'image' => ['nullable',"mimes:jpg,bmp,png,webp"],

        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data = [
            'username' => $this->username,
            'email' => $this->email,
            'password' => $this->password,
        ];
        return array_filter($data, fn ($value) => !is_null($value));
    }
}
