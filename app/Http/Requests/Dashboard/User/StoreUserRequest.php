<?php

namespace App\Http\Requests\Dashboard\User;

use Illuminate\Foundation\Http\FormRequest;


class StoreUserRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'username' => ['required', 'unique:users,username'],
            'email' => ['required', 'string', 'email', 'unique:users,email'],
            'password' => ['required', 'string', 'confirmed'],
            'image' => ['required',"mimes:jpg,bmp,png,webp"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data = [
            'username' => $this->username,
            'email' => $this->email,
            'password' => $this->password,
];
        return $data;
    }
}
