<?php

namespace App\Http\Requests\Dashboard\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $user = $this->route('user');
        return [
            'username' => ['nullable', Rule::unique('users')->ignore($user)],
            'email' => ['nullable', 'string', 'email', Rule::unique('users')->ignore($user)],
            'password' => ['nullable', 'string', 'confirmed'],
            'image' => ['nullable',"mimes:jpg,bmp,png,webp"],

        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data = [
            'username' => $this->username,
            'email' => $this->email,
            'password' => $this->password,
        ];
        return array_filter($data, fn ($value) => !is_null($value));
    }
}
