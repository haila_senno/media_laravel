<?php

namespace App\Http\Requests\Dashboard\Episode;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateEpisodeRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "episode_name" => ["nullable", "string", Rule::unique('episodes')->ignore($this->route('episode'))],
            "episode_number" => [
                "nullable", "numeric",
                Rule::unique('episodes')
                    ->where(function ($query) {
                        return $query->where('show_id', $this->show_id);
                    })
                    ->ignore($this->route('episode'))
            ],
            "show_id" => ["nullable", "exists:shows,id"],
            "file" => ["nullable","mimetypes:video/mp4"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        $data =  [
            "episode_name" => $this->episode_name,
            "episode_number" => $this->episode_number,
            "show_id" => $this->show_id,
        ];
        return array_filter($data, fn ($value) => !is_null($value));
    }
}
