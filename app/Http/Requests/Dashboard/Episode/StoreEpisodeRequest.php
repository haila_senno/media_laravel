<?php

namespace App\Http\Requests\Dashboard\Episode;

use Illuminate\Foundation\Http\FormRequest;


class StoreEpisodeRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "episode_name"=>["required","string",'unique:episodes,episode_name'],
            "episode_number"=>["required","numeric","unique:episodes,episode_number,NULL,NULL,show_id," . $this->show_id],
            "show_id"=>["required","exists:shows,id"],
            "file"=>["required","mimetypes:video/mp4"],
        ];

    }


    public function validated($key = null, $default = null): array
    {
        return [
            "episode_name"=>$this->episode_name,
            "episode_number"=>$this->episode_number,
            "show_id"=>$this->show_id,
        ];
    }

}
