<?php

namespace App\Http\Requests\Dashboard\Game;

use App\Enums\GameTypeEnum;
use App\Enums\ShowYearEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateGameRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "game_name" => ["nullable", "string"],
            "game_type" => ["nullable", new Enum(GameTypeEnum::class)],
            "summery" => ["nullable", "string"],
            "date" => ["nullable", "integer",new Enum(ShowYearEnum::class)],
            "price" => ["nullable","integer",function ($attribute, $value, $fail) {
                if ($this->game_type == GameTypeEnum::FREE->value && $this->price) {
                    $fail('the game is free you can not set price');
                }}],
            "setup" => ["nullable","mimes:zip,rar"],
            "image1" => ["nullable"],"mimes:jpg,bmp,png,webp",
            "image2" => ["nullable","mimes:jpg,bmp,png,webp"],
            "image3" => ["nullable","mimes:jpg,bmp,png,webp"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return [
            "game_name" => $this->game_name,
            "game_type" => $this->game_type,
            "price" => $this->price,
            "summery" => $this->summery,
            "date" => $this->date,
        ];
    }
}
