<?php

namespace App\Http\Requests\Dashboard\Game;

use App\Enums\GameTypeEnum;
use App\Enums\ShowYearEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class StoreGameRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "game_name" => ["required", "string"],
            "game_type" => ["required", new Enum(GameTypeEnum::class)],
            "summery" => ["required", "string"],
            "price" => [Rule::requiredIf($this->game_type == GameTypeEnum::NOT_FREE->value),function ($attribute, $value, $fail) {
                if ($this->game_type == GameTypeEnum::FREE->value && $this->price) {
                    $fail('the game is free you can not set price');
                }}],
            "date" => ["required", "integer",new Enum(ShowYearEnum::class)],
            "setup" => [Rule::requiredIf($this->game_type == GameTypeEnum::NOT_FREE->value),"mimes:zip,rar"],
            "image1" => ["required","mimes:jpg,bmp,png,webp"],
            "image2" => [Rule::requiredIf($this->game_type == GameTypeEnum::NOT_FREE->value),"mimes:jpg,bmp,png,webp"],
            "image3" => [Rule::requiredIf($this->game_type == GameTypeEnum::NOT_FREE->value),"mimes:jpg,bmp,png,webp"],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return [
            "game_name" => $this->game_name,
            "game_type" => $this->game_type,
            "price" => $this->price ?? 0,
            "summery" => $this->summery,
            "date" => $this->date,
        ];
    }
}
