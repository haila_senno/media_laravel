<?php

namespace App\Http\Requests\Guest\Pay;

use Illuminate\Foundation\Http\FormRequest;


class StorePayRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "points" => ["required","numeric"]
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }

}
