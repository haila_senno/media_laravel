<?php

namespace App\Http\Requests\Guest\Rate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreRateRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'value' => ['required', 'numeric', 'min:0', 'max:10']
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return [
            'value' => $this->value,
        ];
    }
}
