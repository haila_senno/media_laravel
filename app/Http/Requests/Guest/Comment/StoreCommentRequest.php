<?php

namespace App\Http\Requests\Guest\Comment;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreCommentRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'text' => ['required','string'],
            'parent_id' => ['nullable','exists:comments,id'],
        ];
    }


    public function validated($key = null, $default = null): array
    {
        $user = Auth::user();
        $data = [
            'text' => $this->text,
            'parent_id' => $this->parent_id,
            'user_id'=>$user->id,
        ];
        return array_filter($data,fn($value) => !is_null($value));
    }

}
