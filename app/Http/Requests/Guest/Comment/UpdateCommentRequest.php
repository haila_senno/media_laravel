<?php

namespace App\Http\Requests\Guest\Comment;

use Illuminate\Foundation\Http\FormRequest;


class UpdateCommentRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            //
        ];
    }


    public function validated($key = null, $default = null): array
    {
        return data_get($this->validator->validated(), $key, $default);
    }

}
