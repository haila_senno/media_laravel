<?php

namespace App\Http\Requests\Guest\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'username'=>['required','unique:users,username'],
            'email' => ['required', 'string', 'email','unique:users,email'],
            'password' => ['required', 'string','confirmed'],
            'birth_date' => ['required', 'date'],
            'image' => ['required',"mimes:jpg,bmp,png,webp"],
        ];
    }

    public function validated($key = null, $default = null): array
    {
        $data = [
            'username'=> $this->username,
            'email' => $this->email,
            'password' => $this->password,
            'birth_date' => $this->birth_date,
        ];
        return array_filter($data,fn($value) => !is_null($value));
    }


}
