<?php

namespace App\Enums;

use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum ShowTypeEnum: int
{
    use Names, Values, Options, InvokableCases;

    case MOVIE = 1;
    case SERIES = 2;

    public static function getTypesForSelect(): array
    {
        return [
            self::MOVIE->value => 'Movie',
            self::SERIES->value => 'Series',
        ];
    }

    public function getHumanName(): string
    {
        return match ($this) {
            self::MOVIE => 'Movie',
            self::SERIES => 'Series',
        };
    }
}
