<?php
namespace App\Enums;
use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum RoleEnum: int
{
    use Names,Values,Options,InvokableCases;

    case ADMIN = 1;
    case USER = 2;
}

