<?php

namespace App\Enums;

use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum ShowYearEnum: int
{
    use Names, Values, Options, InvokableCases;

    case YEAR_2020 = 1;
    case YEAR_2021 = 2;
    case YEAR_2022 = 3;
    case YEAR_2023 = 4;
    case YEAR_2024 = 5;

    public static function getTypesForSelect(): array
    {
        return [
            self::YEAR_2020->value => '2020',
            self::YEAR_2021->value => '2021',
            self::YEAR_2022->value => '2022',
            self::YEAR_2023->value => '2023',
            self::YEAR_2024->value => '2024',
        ];
    }

    public function getHumanName(): string
    {
        return match ($this) {
            self::YEAR_2020 => '2020',
            self::YEAR_2021 => '2021',
            self::YEAR_2022 => '2022',
            self::YEAR_2023 => '2023',
            self::YEAR_2024 => '2024',

        };
    }
}
