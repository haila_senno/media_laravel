<?php
namespace App\Enums;
use ArchTech\Enums\{InvokableCases, Names, Options, Values};

enum TeamMemberTypeEnum: int
{
    use Names,Values,Options,InvokableCases;

    case ACTOR = 1;
    case DIRECTOR = 2;
    case PRODUCER = 3;
    case WRITER = 4;

    public static function getTypesForSelect(): array
    {
        return [
            self::ACTOR->value => 'Actor',
            self::DIRECTOR->value => 'Director',
            self::PRODUCER->value => 'Producer',
            self::WRITER->value => 'Writer',
        ];
    }

    public function getHumanName(): string
    {
        return match ($this) {
            self::ACTOR => 'Actor',
            self::DIRECTOR => 'Director',
            self::PRODUCER => 'Producer',
            self::WRITER => 'Writer',
        };
    }

}

