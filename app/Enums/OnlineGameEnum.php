<?php

namespace App\Enums;

use ArchTech\Enums\InvokableCases;
use ArchTech\Enums\Names;
use ArchTech\Enums\Options;
use ArchTech\Enums\Values;

enum OnlineGameEnum: int
{
    use InvokableCases, Names, Options, Values;

    case SNAKE = 2;
    case SUDOKU = 3;
    case CANDY = 4;

    // public static function getTypesForSelect(): array
    // {
    //     return [
    //         self::FREE->value => 'Free',
    //         self::NOT_FREE->value => 'Not Free',
    //     ];
    // }

    // public function getHumanName(): string
    // {
    //     return match ($this) {
    //         self::FREE => 'Free',
    //         self::NOT_FREE => 'Not Free'
    //     };
    // }

}
