<?php

namespace App\Enums;

use ArchTech\Enums\InvokableCases;
use ArchTech\Enums\Names;
use ArchTech\Enums\Options;
use ArchTech\Enums\Values;

enum GameTypeEnum: int
{
    use InvokableCases, Names, Options, Values;

    case FREE = 1;
    case NOT_FREE = 2;

    public static function getTypesForSelect(): array
    {
        return [
            self::FREE->value => 'Free',
            self::NOT_FREE->value => 'Not Free',
        ];
    }

    public function getHumanName(): string
    {
        return match ($this) {
            self::FREE => 'Free',
            self::NOT_FREE => 'Not Free'
        };
    }

}
