<?php

namespace App\Services;

use Illuminate\Support\Str;
use Spatie\MediaLibrary\Support\MediaStream;

class ShowService
{
    public static function attach($model,$user)
    {
        $user->shows()->attach($model->id, ['price' => $model->price]);
    }

    public static function download($model,$user)
    {
        $media = collect();
        foreach ($model->episodes as $episode) {
            $media = $media->merge($episode->getMedia('Episode'));
        }

        return MediaStream::create('episodes.zip')->addMedia($media);    }
}
