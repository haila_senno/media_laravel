<!-- resources/views/vendor/pagination/custom.blade.php -->
@if ($paginator->hasPages())
    <div class="col-12">
        <ul class="paginator">
            {{-- Previous Page Link --}}
            <li class="paginator__item paginator__item--prev {{ $paginator->onFirstPage() ? 'disabled' : '' }}">
                @if ($paginator->onFirstPage())
                    <span><i class="icon ion-ios-arrow-back"></i></span>
                @else
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="icon ion-ios-arrow-back"></i></a>
                @endif
            </li>

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="paginator__item disabled"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="paginator__item paginator__item--active"><a href="#" class="page-link">{{ $page }}</a></li>
                        @else
                            <li class="paginator__item"><a href="{{ $url }}" class="page-link">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            <li class="paginator__item paginator__item--next {{ $paginator->hasMorePages() ? '' : 'disabled' }}">
                @if ($paginator->hasMorePages())
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="icon ion-ios-arrow-forward"></i></a>
                @else
                    <span><i class="icon ion-ios-arrow-forward"></i></span>
                @endif
            </li>
        </ul>
    </div>
@endif
