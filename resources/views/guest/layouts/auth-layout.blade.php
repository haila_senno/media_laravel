<x-guest-layout::app-layout>
    <div class="sign section--bg" data-bg="img/section/section.jpg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sign__content">
                        <!-- authorization form -->
                       {{$slot}}
                        <!-- end authorization form -->
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-guest-layout::app-layout>
