<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{$title}}</title>

	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CUbuntu:300,400,500,700" rel="stylesheet">

	<!-- CSS -->
	<link rel="stylesheet" href={{asset('/guest/assets/css/bootstrap-reboot.min.css')}}>
	<link rel="stylesheet" href={{asset('/guest/assets/css/bootstrap-grid.min.css')}}>

	<link rel="stylesheet" href={{asset('/guest/assets/css/owl.carousel.min.css')}}>
	<link rel="stylesheet" href={{asset('/guest/assets/css/jquery.mCustomScrollbar.min.css')}}>
	<link rel="stylesheet" href={{asset('/guest/assets/css/nouislider.min.css')}}>
	<link rel="stylesheet" href={{asset('/guest/assets/css/ionicons.min.css')}}>
	<link rel="stylesheet" href={{asset('/guest/assets/css/plyr.css')}}>
	<link rel="stylesheet" href={{asset('/guest/assets/css/photoswipe.css"')}}>
	<link rel="stylesheet" href={{asset('/guest/assets/css/default-skin.css')}}>
	<link rel="stylesheet" href={{asset('/guest/assets/css/main.css')}}>

	<!-- Favicons -->
	<link rel="icon" type="image/png" href="icon/favicon-32x32.png" sizes="32x32">
	<link rel="apple-touch-icon" href="icon/favicon-32x32.png">
	<link rel="apple-touch-icon" sizes="72x72" href={{asset('/guest/assets/icon/apple-touch-icon-72x72.png')}}>
	<link rel="apple-touch-icon" sizes="114x114" href={{asset('/guest/assets/icon/apple-touch-icon-114x114.png')}}>
	<link rel="apple-touch-icon" sizes="144x144" href={{asset('/guest/assets/icon/apple-touch-icon-144x144.png')}}>


    {{$head ?? ''}}

	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Dmitry Volkov">
	<title>FlixGo – Online Movies, TV Shows & Cinema HTML Template</title>

</head>
<body class="body">

	{{$slot}}

	<!-- JS -->
	<script src={{asset('/guest/assets/js/jquery-3.3.1.min.js')}}></script>
	<script src={{asset('/guest/assets/js/bootstrap.bundle.min.js')}}></script>
	<script src={{asset('/guest/assets/js/owl.carousel.min.js')}}></script>
	<script src={{asset('/guest/assets/js/jquery.mousewheel.min.js')}}></script>
	<script src={{asset('/guest/assets/js/jquery.mCustomScrollbar.min.js')}}></script>
	<script src={{asset('/guest/assets/js/wNumb.js')}}></script>
	<script src={{asset('/guest/assets/js/nouislider.min.js')}}></script>
	<script src={{asset('/guest/assets/js/plyr.min.js')}}></script>
	<script src={{asset('/guest/assets/js/jquery.morelines.min.js')}}></script>
	<script src={{asset('/guest/assets/js/photoswipe.min.js')}}></script>
	<script src={{asset('/guest/assets/js/photoswipe-ui-default.min.js')}}></script>
	<script src={{asset('/guest/assets/js/main.js')}}></script>


    {{$script ?? ''}}
</body>

</html>
