<x-guest-layout::app-layout :title="$title">
    <x-slot name="head">
        {{ $head ?? '' }}
    </x-slot>
    <x-slot name="script">
        {{ $script ?? '' }}
    </x-slot>

   <!-- header -->
	<header class="header">
		<div class="header__wrap">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="header__content">
							<!-- header logo -->
							<div class="logo--area" style="min-width: 250px; max-width: 250px;">
								<h1>
									<a href="{{route('home')}}" title="">
										<div class="en-text" style="color:#ff55a5  ;">
											<span>
												Just4fun
											</span>
										</div>
									</a>
								</h1>
							</div>
							<!-- end header logo -->

							<!-- header nav -->
							<ul class="header__nav">
								<!-- dropdown -->
								<li class="header__nav-item">
									<a class=" header__nav-link" href="{{route('home')}}" >Home</a>


								</li>
								<!-- end dropdown -->

								<!-- dropdown -->
								<li class="header__nav-item">
									<a class="dropdown-toggle header__nav-link" href="#" role="button" id="dropdownMenuCatalog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Content</a>

									<ul class="dropdown-menu header__dropdown-menu" aria-labelledby="dropdownMenuCatalog">
										<li><a href="{{route('user.movie.index')}}">Movies</a></li>
										<li><a href="{{route('user.series.index')}}">TV Series</a></li>
										<li><a href="{{route('user.game.index')}}">Download Games</a></li>
                                    <li><a href="{{route('user.game.online.index')}}">Online Games</a></li>

									</ul>
								</li>
								<!-- end dropdown -->

								<li class="header__nav-item">
									<a href="{{route('about')}}" class="header__nav-link">About</a>
								</li>

								<li class="header__nav-item">
									<a href="{{route('help')}}" class="header__nav-link">Help</a>
								</li>

								<!-- dropdown -->
								<li class="dropdown header__nav-item">
									<a class="dropdown-toggle header__nav-link header__nav-link--more" href="#" role="button" id="dropdownMenuMore" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon ion-ios-more"></i></a>

									<ul class="dropdown-menu header__dropdown-menu" aria-labelledby="dropdownMenuMore">
                                        @guest
                                        <li><a href="{{route('user.login')}}">Sign In</a></li>
										<li>
                                        @endguest
                                            @auth
                                            <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log Out</a>

                                            <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>

                                        </li>

                                        @if (auth()->user()->hasRole(App\Enums\RoleEnum::ADMIN->value))
										<li><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        @endif
                                        <li><a href="{{route('user.show')}}">My Account</a></li>
                                        @endauth


									</ul>
								</li>
								<!-- end dropdown -->
							</ul>
							<!-- end header nav -->

							<!-- header auth -->
							<div class="header__auth">
                                @guest
                                <a href="{{route('user.login')}}" class="header__sign-in">
									<i class="icon ion-ios-log-in"></i>
									<span>sign in</span>
								</a>
                                @endguest
                                @auth
                                <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();"  class="header__sign-in">
									<i class="icon ion-ios-log-in"></i>
									<span>Log Out</span>
                                    <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
								</a>



                                <a href="{{route('user.show')}}" class="nav-link ">
                                    <img class="rounded-circle me-lg-2" src={{auth()->user()->hasRole(App\Enums\RoleEnum::USER->value) ? auth()->user()->getFirstMediaUrl('User') : auth()->user()->getFirstMediaUrl('Admin')}} alt="" style="width: 40px; height: 40px;">
                                    <span style="text-transform:none" class="d-none d-lg-inline-flex header__nav-link">{{auth()->user()->username}}</span>
                                </a>
                                @endauth
							</div>

							<!-- end header auth -->
						</div>
					</div>
				</div>
			</div>
		</div>

	</header>
	<!-- end header -->

	{{$slot}}

<!-- footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <!-- footer list -->
            <div class="col-12 col-md-3">

            </div>
            <!-- end footer list -->

            <!-- footer list -->
            <div class="col-6 col-sm-4 col-md-3">
                <h6 class="footer__title">Resources</h6>
                <ul class="footer__list">
                    <li><a href="{{route('about')}}">About Us</a></li>
                    <li><a href="{{route('user.pay.index')}}">Purchase points</a></li>
                    <li><a href="{{route('help')}}">Help</a></li>
                </ul>
            </div>
            <!-- end footer list -->

            <!-- footer list -->
            <div class="col-6 col-sm-4 col-md-3">
                <h6 class="footer__title">Legal</h6>
                <ul class="footer__list">
                    <li><a href="#">Terms of Use</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Security</a></li>
                </ul>
            </div>
            <!-- end footer list -->

            <!-- footer list -->
            <div class="col-12 col-sm-4 col-md-3">


            </div>
            <!-- end footer list -->

            <!-- footer copyright -->
            <div class="col-12">
                <div class="footer__copyright">
                    <small><a target="_blank" href="https://www.templateshub.net"> </a></small>

                    <ul>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <!-- end footer copyright -->
        </div>
    </div>
</footer>
<!-- end footer -->

	<!-- end footer -->
</x-guest-layout-layout::app-layout>
