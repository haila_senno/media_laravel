<x-guest-layout::guest-layout>
    <x-slot name="title">
        My Account
    </x-slot>



    <!-- details -->
	<section class="section details">
		<!-- details background -->
		<div class="details__bg" data-bg={{asset('guest/assets/img/home/home__bg.jpg')}}></div>
		<!-- end details background -->

		<!-- details content -->
		<div class="container">
			<div class="row">
				<!-- title -->
				<div class="col-12">
					<h1 class="details__title" style="color:#ff55a5">My Account</h1>
				</div>
				<!-- end title -->

				<!-- content -->
                {{-- <div class="center"> --}}
                    <div class="col-12 col-xl-10">
                        <div class="card card--details">
                            <div class="row">
                                <!-- card cover -->
                                <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-5">
                                    <div >
                                        <div class="card__cover">
                                            <img style="background-size: cover;
                                            /* object-fit: cover; */

                                            border-radius: 60%;
                                          " src="{{$user->getFirstMediaUrl('User')}}" alt="" >
                                          <br>
                                          <br>
                                        </div>
                                        <div class="price__item--first"><span>Total Points : </span> <span>{{auth()->user()->points}}</span></div>
                                    </div>
                                </div>
                                <!-- end card cover -->

                                <!-- card content -->
                                <div class="col-12 col-sm-8 col-md-8 col-lg-9 col-xl-7">
                                    <form action="{{ route('user.update', $user) }}" method="POST" class="sign__form" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')

                                        <div class="sign__group">
                                            <div class="logo--area">
                                                <p style="color:#ff55a5" >User Name</p>
                                            </div>
                                            {{-- <input name="image" type="file" class="file-upload"> --}}
                                            <input value="{{$user->username}}" name="username" type="text" class="sign__input" placeholder="User Name" value="{{old('username')}}">
                                            @error('username')
                                            <div style="color: #dc3545;" class="text-danger">{{$message}}</div>
                                            @enderror
                                        </div>

                                        <div class="sign__group">
                                            <div class="logo--area">
                                                <p style="color:#ff55a5">Email</p>
                                            </div>
                                            <input value="{{$user->email}}" name="email" type="email" class="sign__input" placeholder="Email" value="{{old('email')}}">
                                            @error('email')
                                            <div style="color: #dc3545;" class="text-danger">{{$message}}</div>
                                            @enderror
                                        </div>

                                        <div class="sign__group">
                                            <div class="logo--area">
                                                <p style="color:#ff55a5">New Password</p>
                                            </div>
                                            <input name="password" type="password" class="sign__input" placeholder="Password">
                                            @error('password')
                                            <div style="color: #dc3545;" class="text-danger">{{$message}}</div>
                                            @enderror
                                        </div>

                                        <div class="sign__group">
                                            <div class="logo--area">
                                                <p style="color:#ff55a5">Confirm Password</p>
                                            </div>
                                            <input name="password_confirmation" type="password" class="sign__input" placeholder="Confirm Password">
                                            @error('password_confirmation')
                                            <div style="color: #dc3545;" class="text-danger">{{$message}}</div>
                                            @enderror
                                        </div>
                                        <div >
                                            <div class="logo--area">
                                                <p style="color:#ff55a5">Upload Image</p>
                                            </div>
                                            <input  name="image" type="file" class="sign__input" placeholder="Confirm Password">
                                            @error('image')
                                            <div style="color: #dc3545;" class="text-danger">{{$message}}</div>
                                            @enderror
                                        </div>

                                        <button class="sign__btn" type="submit">Edit</button>
                                    </form>
                                </div>
                                <!-- end card content -->
                            </div>
                        </div>
                    </div>
                {{-- </div> --}}


				<!-- end content -->


			</div>
		</div>
		<!-- end details content -->
	</section>
	<!-- end details -->
</x-guest-layout::guest-layout>
