<x-guest-layout::guest-layout>
    <x-slot name="title">
        Pay
    </x-slot>

    <section class="section details">
        <!-- details background -->
        <div class="details__bg" data-bg={{ asset('guest/assets/img/home/home__bg.jpg') }}></div>
        <!-- end details background -->

        <!-- details content -->
        <div class="container">
            <div class="row">
                <!-- title -->
                <div class="col-12">
                    <h1 class="details__title">Purchase Points</h1>
                </div>
                <!-- end title -->
                <!-- accordion -->
                <div class="col-12 col-xl-14">
                    <div class="accordion" id="accordion">
                        <div class="accordion__card">
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <table class="accordion__list">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Points</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($settings as $setting)
                                                <tr>
                                                    <td>{{ $setting->id }}</td>
                                                    <td>{{ $setting->points }}</td>
                                                    <td>${{ $setting->price }}</td>
                                                    <td>
                                                        <button onclick="showPayModal({{$setting->id}})" class="form__btn"
                                                            style="height: 40px;
                                                            width: 60px;
                                                            margin: 25px 0 0 0
                                                             -webkit-border-radius: 10px;
                                                              border-radius: 10px;;"
                                                              >Buy</button>
                                                              <div id="payModal{{ $setting->id }}" class="modal">
                                                                <div id="payModalContent">
                                                                    <form method="POST" action="{{route('user.pay.buy',$setting->id)}}">
                                                                        @csrf
                                                                        <h1 style="color: #ff55a5">Buy Points</h1>
                                                                        <hr>
                                                                    <div style="color: black" class="price__item--first"><h5>Are you sure to buy </h5><h5>{{$setting->points}} points ?</h5></div>
                                                                    <div style="color: black"><span>we will send you an email when Admin accept your order</span></div>
                                                                    <br>
                                                                    <div style="color: black" ><p>check your inbox please</p></div>
                                                                    <div class="modal-buttons">
                                                                        <button class="form__btn" type="submit">Buy</button>
                                                                        <button type="button" onclick="closeModal({{ $setting->id }})" class="form__btn">close</a>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="price__item--first"><span>Your Points : </span> <span>{{auth()->user()->points}}</span></div>
                </div>
                <!-- end accordion -->


            </div>
        </div>
        <!-- end details content -->
    </section>
</x-guest-layout::guest-layout>
