<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0 user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Sudoku</title>

    <link rel="stylesheet" href={{ asset('guest/assets/css/sudoku.css') }}>

    <script src={{ asset('guest/assets/js/sudoku.js') }}></script>
    <script>
        var playRoute = "{{ route('user.game.online.play', $game->id) }}"
        var level = {{ $level }}
        var userPointsRoute = "{{ route('user.game.online.userPoints') }}"
    </script>
</head>

<body>
    <h1 style="color: #ff55a5">{{$game->game_name}}</h1>
    <hr>
    <div class="price__item--first"><span style="color: black">errors : </span><span id="errors">0</span></div>
    <div class="price__item--first"><span style="color: black">score : </span><span id="score">0</span></div>
    <div id="gameOverModal">
        <div id="gameOverModalContent">
            <h1>Game Over</h1>
            <p>You are in level : <span id="level"></span></p>
            <p>Your total points is : <span id="user-points"></span></p>
            <div class="modal-buttons">
                <button style="border:0px" class="form__btn" onclick="reloadGame()">Play Again</button>
                <a href="{{ route('home') }}" class="form__btn">Go Home</a>
            </div>
        </div>
    </div>
    <!-- 9x9 board -->
    <div id="board"></div>
    <br>
    <div id="digits">
    </div>


</body>


</html>
