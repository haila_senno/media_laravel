<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport", content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Snake</title>
    <link rel="stylesheet" href={{ asset('guest/assets/css/snake.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/main.css') }}>

    <script src={{ asset('guest/assets/js/snake.js') }}></script>
    <script>
        var playRoute = "{{ route('user.game.online.play', $game->id) }}"
        var level = {{$level}}
        var userPointsRoute = "{{ route('user.game.online.userPoints') }}"
    </script>
</head>

<body>
    <h1 style="color: #ff55a5">{{$game->game_name}}</h1>
    <div class="price__item--first"><span>Your score now is : </span><span id="points">0</span></div>
    <div id="gameOverModal">
        <div id="gameOverModalContent">
            <h1>Game Over</h1>
            <p>You are in level : <span id="level"></span></p>
            <p>Your total points is : <span id="user-points"></span></p>
            <div class="modal-buttons">
                <button class="form__btn" onclick="reloadGame()">Play Again</button>
                <a href="{{ route('home') }}" class="form__btn">Go Home</a>
            </div>

        </div>
    </div>
     <canvas id="board"></canvas>
</body>

</html>
