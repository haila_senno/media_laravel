
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Candy Crush</title>
    <link rel="stylesheet" href={{asset('guest/assets/css/candy.css')}} />
    <link rel="stylesheet" href={{asset('guest/assets/css/main.css')}} />

    <script src={{asset('guest/assets/js/candy.js')}}></script>
    <script>
        var baseUrl = "{{ asset('guest/assets/img') }}/";
        var playRoute = "{{route('user.game.online.play',$game->id)}}";
        var level = {{$level}}
        var userPointsRoute = "{{ route('user.game.online.userPoints') }}"
      </script>
  </head>

  <body>
    <h1 style="color: #ff55a5">{{$game->game_name}}</h1>
    <div class="price__item--first"><span>Your score now is : </span><span id="score">0</span></div>
    <div id="board"></div>
    <div class="button-container">
        <button class="form__btn" onclick="showGameOver()">See Your Points</button>
    </div>
    <div id="gameOverModal">
        <div id="gameOverModalContent">
            <h1>Great !!</h1>
            <div style="color: black" class="price__item--first"><span>You are in level : </span><span id="level"></span></div>
            <div style="color: black"  class="price__item--first"><span>Your total points is : </span><span id="user-points" ></span></div>
            <div class="modal-buttons">
                <button class="form__btn" onclick="reloadGame()">Play Again</button>
                <a href="{{ route('home') }}" class="form__btn">Go Home</a></div>
        </div>
    </div>
  </body>
</html>
