<x-guest-layout::guest-layout>
    <x-slot name="title">
        Online Games
    </x-slot>
    <x-slot name="head">
        <style>
            .filter-select {
                -webkit-box-shadow: 0 5px 25px 0 rgba(0, 0, 0, 0.3);
                background-color: #2b2b31;
                box-shadow: 0 5px 25px 0 rgba(0, 0, 0, 0.3);
                /* Change the background color */
                color: #343a40;
                /* Change the text color */
                /* border: 1px solid #ced4da; */
                /* Change the border color */
                border-radius: 0.25rem;
                /* Change the border radius */
                padding: 0.375rem 0.75rem;
                /* Change the padding */
                font-size: 1rem;
                color: #fff;
                font-family: Arial, sans-serif;

            }

            .filter-select>option {
                display: block;
                line-height: 40px;
                font-size: 14px;
                color: rgba(255, 255, 255, 0.75);
                position: relative;
                font-weight: 300;
                cursor: pointer;
                -webkit-transition: 0.4s ease;
                -moz-transition: 0.4s ease;
                transition: 0.4s ease;
            }

            .filter-select>option:hover {
                color: #ff55a5;

            }

            .custom-select option {
                background-color: #ffffff;
                /* Change the background color of options */
                color: #495057;
                /* Change the text color of options */
            }

            .custom-select option:hover {
                background-color: #e9ecef;
                /* Hover background color */
                color: #007bff;
                /* Hover text color */
            }

            .custom-select option:checked {
                background-color: #007bff;
                /* Change the background color of selected option */
                color: #ffffff;
                /* Change the text color of selected option */
            }

            .custom-select::after {
                content: "▼";
                position: absolute;
                right: 1rem;
                pointer-events: none;
                color: #495057;
            }
        </style>
    </x-slot>
    <!-- page title -->
    <section class="section section--first section--bg" data-bg={{ asset('guest/assets/img/section/section.jpg') }}>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section__wrap">
                        <!-- section title -->
                        <h2 class="section__title">Online Games</h2>
                        <!-- end section title -->

                        <!-- breadcrumb -->
                        <ul class="breadcrumb">
                            <li class="breadcrumb__item"><a href={{route('home')}}>Home</a></li>
                            <li class="breadcrumb__item breadcrumb__item--active">Online Games</li>
                        </ul>
                        <!-- end breadcrumb -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end page title -->

    <div class="filter">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <form action="{{ route('user.game.online.index') }}" method="get">
                        <div class="filter__content">
                            <div class="filter__items">
                                <!-- filter item -->
                                <div class="filter__item" id="filter__year">
                                    <span class="filter__item-label">RELEASE YEAR:</span>
                                    <select class="filter-select" style="" class="form-select form-select-sm mb-5"
                                        required="" aria-label="s.form-select-sm example" name="filter[date]">
                                        <option selected disabled>Select Year</option>
                                        @foreach ($years as $key => $year)
                                            <option @selected(request()->input('filter.date') == $key) value="{{ $key }}">
                                                {{ $year }}</option>
                                            {{-- <li value="{{ $category->id }}">{{ $category->category_type }}</li> --}}
                                        @endforeach
                                    </select>
                                </div>
                                <!-- end filter item -->

                                <!-- filter item -->
                                <div class="filter__item" id="filter_name">
                                    <span class="filter__item-label">GAME NAME:</span>
                                    <input value="{{ request()->input('filter.game_name') }}" name="filter[game_name]"
                                        type="text" class="sign__input" placeholder="Game1:ex">
                                </div>
                                <!-- end filter item -->
                            </div>
                            <!-- filter btn -->
                            <button class="filter__btn" type="submit">apply filter</button>
                            <!-- end filter btn -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- catalog -->
    <div class="catalog">
        <div class="container">
            <div class="row">
                <!-- card -->
                @foreach ($games as $game)
                    <!-- card -->
                    <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
                        <div class="card">
                            <div class="card__cover">
                                <img style="background-size: cover;
                            object-fit: cover;
                            width: 200px
                            height: 300px
                            aspect-ratio: 3/4;
                            aspect-ratio: 3/4;"
                                    src="{{ $game->getFirstMediaUrl('Game1') }}" alt="">
                                <a href="{{ route('user.game.online.show', $game->id) }}" class="card__play">
                                    <i class="icon ion-ios-play"></i>
                                </a>
                            </div>
                            <div class="card__content">
                                <h3 class="card__title"><a href="#">{{ $game->game_name }}</a></h3>
                                <span class="card__category">
                                    <a href="#">
                                        @switch($game->date)
                                            @case(App\Enums\ShowYearEnum::YEAR_2020->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2020->getHumanName() }}
                                            @break

                                            @case(App\Enums\ShowYearEnum::YEAR_2021->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2021->getHumanName() }}
                                            @break

                                            @case(App\Enums\ShowYearEnum::YEAR_2022->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2022->getHumanName() }}
                                            @break

                                            @case(App\Enums\ShowYearEnum::YEAR_2023->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2023->getHumanName() }}
                                            @break

                                            @case(App\Enums\ShowYearEnum::YEAR_2024->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2024->getHumanName() }}
                                            @break
                                        @endswitch
                                    </a>
                                </span>
                                <span class="card__rate"><i class="icon ion-ios-star"></i>
                                    @switch($game->game_type)
                                        @case(App\Enums\GameTypeEnum::FREE->value)
                                            {{ App\Enums\GameTypeEnum::FREE->getHumanName() }}
                                        @break

                                        @case(App\Enums\GameTypeEnum::NOT_FREE->value)
                                            {{ App\Enums\GameTypeEnum::NOT_FREE->getHumanName() }}
                                        @break
                                    @endswitch
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- end card -->
                @endforeach

                <!-- paginator -->

                {{ $games->links('vendor.pagination.custom') }}

                <!-- end paginator -->

                <!-- paginator -->

                <!-- end paginator -->
            </div>
        </div>
    </div>
    <!-- end catalog -->



</x-guest-layout::guest-layout>
