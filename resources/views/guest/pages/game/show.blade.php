<x-guest-layout::guest-layout>
    <x-slot name="title">
        Show Game
    </x-slot>
    <!-- details -->
    <section class="section details">
        <!-- details background -->
        <div class="details__bg" data-bg={{ asset('guest/assets/img/home/home__bg.jpg') }}></div>
        <!-- end details background -->

        <!-- details content -->
        <div class="container">
            <div class="row">
                <!-- title -->
                <div class="col-12">
                    <h1 class="details__title">{{ $game->game_name }}</h1>
                </div>
                <!-- end title -->

                <!-- content -->
                <div class="col-12 col-xl-6">
                    <div class="card card--details">
                        <div class="row">
                            <!-- card cover -->
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-5 " >
                                <div class="card__cover">
                                    <img src={{ $game->getFirstMediaUrl('Game1') }} alt="">
                                </div>
                            </div>
                            <!-- end card cover -->

                            <!-- card content -->
                            <div class="col-12 col-sm-8 col-md-8 col-lg-9 col-xl-7">
                                <div class="card__content">
                                    <div class="card__wrap">
                                        <span class="card__rate"><i class="icon ion-ios-star"></i>
                                            @switch($game->game_type)
                                                @case(App\Enums\GameTypeEnum::FREE->value)
                                                    {{ App\Enums\GameTypeEnum::FREE->getHumanName() }}
                                                @break

                                                @case(App\Enums\GameTypeEnum::NOT_FREE->value)
                                                    {{ App\Enums\GameTypeEnum::NOT_FREE->getHumanName() }}
                                                @break
                                            @endswitch
                                        </span>
                                    </div>

                                    <ul class="card__meta">


                                        <li><span>Release year:</span>
                                            @switch($game->date)
                                                @case(App\Enums\ShowYearEnum::YEAR_2020->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2020->getHumanName() }}
                                                @break

                                                @case(App\Enums\ShowYearEnum::YEAR_2021->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2021->getHumanName() }}
                                                @break

                                                @case(App\Enums\ShowYearEnum::YEAR_2022->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2022->getHumanName() }}
                                                @break

                                                @case(App\Enums\ShowYearEnum::YEAR_2023->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2023->getHumanName() }}
                                                @break

                                                @case(App\Enums\ShowYearEnum::YEAR_2024->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2024->getHumanName() }}
                                                @break
                                            @endswitch
                                        </li>
                                        <li><span>Price:</span>
                                            {{$game->price}}

                                    </ul>

                                    <div class="card__description card__description--details">
                                        {{ $game->summery }}
                                    </div>
                                </div>
                            </div>
                            <!-- end card content -->
                        </div>
                    </div>
                </div>
                <!-- end content -->

                <!-- player -->
                <div class="card__description Requirements card__description Requirements--details"
                    style="padding:0 5px; color:#777; font-size: larger;">

                    <li><span></span> </span>The Requirements Needed To Download The Game:</li>
                    <li><span></span> CPU: Core i5-7600K / AMD Ryzen 5 1600</li>
                    <li><span></span> Graphics card: GeForce RTX 2060 / Radeon RX 6600 6GB</li>
                    <li><span></span> RAM: 16 GB </li>
                    @if(auth()->user()->points < $game->points)
                    <button class="form__btn" disabled>Download</button>
                    <div>
                        <span  class="card__rate"  style="color: rgba(255,255,255,0.75);">You have less than {{ $game->points }} points</span>
                        <br>
                        <span class="card__rate" style="color: rgba(255,255,255,0.75);">Go To </span><a id="pay-button" href="{{ route('user.pay.index') }}"> Pay Page</a>
                    </div>
                    @else
                    <form action="{{ route('user.game.download',$game->id) }}" method="GET">
                        <button type="submit" class="form__btn">download</button>
                    </form>
                    @endif

                </div>
                <!-- end player -->

                <div class="col-12">
                    <div class="details__wrap">
                        <!-- availables -->
                        <div class="details__devices">

                        </div>
                        <!-- end availables -->

                    </div>
                </div>
            </div>
        </div>
        <!-- end details content -->
    </section>
    <!-- end details -->
</x-guest-layout::guest-layout>
