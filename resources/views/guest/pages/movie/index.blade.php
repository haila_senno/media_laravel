<x-guest-layout::guest-layout>
    <x-slot name="title">
        Movies
    </x-slot>
    <!-- page title -->
    <x-slot name="head">
        <style>
            .filter-select {
                -webkit-box-shadow: 0 5px 25px 0 rgba(0, 0, 0, 0.3);
                background-color: #2b2b31;
                box-shadow: 0 5px 25px 0 rgba(0, 0, 0, 0.3);
                /* Change the background color */
                color: #343a40;
                /* Change the text color */
                /* border: 1px solid #ced4da; */
                /* Change the border color */
                border-radius: 0.25rem;
                /* Change the border radius */
                padding: 0.375rem 0.75rem;
                /* Change the padding */
                font-size: 1rem;
                color: #fff;
                font-family: Arial, sans-serif;

            }

            .filter-select>option {
                display: block;
                line-height: 40px;
                font-size: 14px;
                color: rgba(255, 255, 255, 0.75);
                position: relative;
                font-weight: 300;
                cursor: pointer;
                -webkit-transition: 0.4s ease;
                -moz-transition: 0.4s ease;
                transition: 0.4s ease;
            }

            .filter-select>option:hover {
                color: #ff55a5;

            }

            .custom-select option {
                background-color: #ffffff;
                /* Change the background color of options */
                color: #495057;
                /* Change the text color of options */
            }

            .custom-select option:hover {
                background-color: #e9ecef;
                /* Hover background color */
                color: #00ff59;
                /* Hover text color */
            }

            .custom-select option:checked {
                background-color: #007bff;
                /* Change the background color of selected option */
                color: #ffffff;
                /* Change the text color of selected option */
            }

            .custom-select::after {
                content: "▼";
                position: absolute;
                right: 1rem;
                pointer-events: none;
                color: #495057;
            }
        </style>
    </x-slot>
    <section class="section section--first section--bg" data-bg={{ asset('guest/assets/img/section/section.jpg') }}>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section__wrap">
                        <!-- section title -->
                        <h2 class="section__title">Movies</h2>
                        <!-- end section title -->

                        <!-- breadcrumb -->
                        <ul class="breadcrumb">
                        <li class="breadcrumb__item"><a href={{route('home')}}>Home</a></li>
                        <li class="breadcrumb__item breadcrumb__item--active">Movies</li>
                        </ul>
                        <!-- end breadcrumb -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end page title -->

    <div class="filter">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <form action="{{ route('user.movie.index') }}" method="get">


                        <div class="filter__content">
                            <div class="filter__items">
                                <div class="filter__item" id="filter__genre">
                                    <span class="filter__item-label">GENRE:</span>

                                    <select class="filter-select" class="form-select form-select-sm mb-3" required=""
                                        aria-label="s.form-select-sm example" name="filter[category_id]">
                                        <option selected disabled>Select Genre</option>
                                        @foreach ($categories as $category)
                                            <option @selected(request()->input('filter.category_id') == $category->id) value="{{ $category->id }}">{{ $category->category_type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- filter item -->
                                <div class="filter__item" id="filter__year">
                                    <span class="filter__item-label">RELEASE YEAR:</span>
                                    <select class="filter-select" style="" class="form-select form-select-sm mb-5"
                                        required="" aria-label="s.form-select-sm example" name="filter[release_date]">
                                        <option selected disabled>Select Year</option>
                                        @foreach ($years as $key => $year)
                                            <option @selected(request()->input('filter.release_date') == $key) value="{{ $key }}">
                                                {{ $year }}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <!-- end filter item -->
                                <div class="filter__item" id="filter__rate">
                                    <span class="filter__item-label"> RATE:</span>

                                      <div class="filter__item-btn dropdown-toggle" role="button" id="filter-rate"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <div class="filter__range">
                                            <div id="filter__imbd-start">{{ request()->input('filter.min_rate') }}</div>
                                            <div id="filter__imbd-end">{{ request()->input('filter.max_rate') }}</div>
                                            <input hidden name="filter[min_rate]" value="{{ request()->input('filter.min_rate') }}" id="min_rate">
                                            <input hidden name="filter[max_rate]" value="{{ request()->input('filter.max_rate') }}" id="max_rate">
                                        </div>
                                        <span></span>
                                    </div>

                                    <div class="filter__item-menu filter__item-menu--range dropdown-menu"
                                        aria-labelledby="filter-rate">
                                        <div id="filter__imbd"></div>
                                    </div>

                                </div>
                                <!-- filter item -->
                                <div class="filter__item" id="filter_name">
                                    <span class="filter__item-label">Movie NAME:</span>
                                    <input value="{{request()->input('filter.show_name')}}" name="filter[show_name]" type="text" class="sign__input" placeholder="Movie1:ex" >
                                </div>
                                <!-- end filter item -->


                            </div>

                            <!-- filter btn -->
                            <button class="filter__btn" type="submit">apply filter</button>
                            <!-- end filter btn -->

                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

    {{-- <!-- filter -->
    <div class="filter">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <form action="{{ route('user.movie.index') }}">
                        <div class="filter__content">
                            <div class="filter__items">
                                <!-- filter item -->
                                <div class="filter__item" id="filter__genre">
                                    <span class="filter__item-label">GENRE:</span>

                                    <select class="filter-select" class="form-select form-select-sm mb-3" required=""
                                        aria-label="s.form-select-sm example" name="filter[category_id]">
                                        <option selected disabled>Select Genre</option>
                                        @foreach ($categories as $category)
                                            <option @selected(request()->input('filter.category_id') == $category->id) value="{{ $category->id }}">{{ $category->category_type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- end filter item -->
                                <!-- filter item -->
                                <div class="filter__item" id="filter__year">
                                    <span class="filter__item-label">RELEASE YEAR:</span>

                                    <select style="" class="filter-select form-select form-select-sm mb-5" required=""
                                        aria-label="s.form-select-sm example" name="filter[release_release_date]">
                                        <option selected disabled>select year</option>
                                        @foreach ($years as $key => $year)
                                            <option @selected(request()->input('filter.release_date') == $key) value="{{ $key }}">{{ $year }}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <!-- end filter item -->
                                <!-- filter item -->
                                <div class="filter__item" id="filter__rate">
                                    <span class="filter__item-label"> RATE:</span>

                                     <div class="filter__item-btn dropdown-toggle" role="button" id="filter-rate"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <div class="filter__range">
                                            <div id="filter__imbd-start">{{ request()->input('filter.min_rate') }}</div>
                                            <div id="filter__imbd-end">{{ request()->input('filter.max_rate') }}</div>
                                            <input hidden name="filter[min_rate]" value="{{ request()->input('filter.min_rate') }}" id="min_rate">
                                            <input hidden name="filter[max_rate]" value="{{ request()->input('filter.max_rate') }}" id="max_rate">
                                        </div>
                                        <span></span>
                                    </div>

                                    <div class="filter__item-menu filter__item-menu--range dropdown-menu"
                                        aria-labelledby="filter-rate">
                                        <div id="filter__imbd"></div>
                                    </div>

                                </div>
                                <div class="filter__item" id="filter_name">
                                    <span class="filter__item-label">MOVIE NAME:</span>
                                    <input value="{{request()->input('filter.show_name')}}" name="filter[show_name]" type="text" class="sign__input" placeholder="Movie1:ex" >
                                </div>
                                <!-- end filter item -->
                            </div>

                            <!-- filter btn -->
                            <button class="filter__btn" type="submit">apply filter</button>
                            <!-- end filter btn -->

                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <!-- end filter --> --}}

    <!-- catalog -->
    <div class="catalog">
        <div class="container">

            <div class="row">
                @foreach ($movies as $movie)
                    <!-- card -->
                    <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
                        <div class="card">
                            <div class="card__cover">
                                <img style="background-size: cover;
                            object-fit: cover;
                            width: 200px
                            height: 300px
                            aspect-ratio: 3/4;
                            aspect-ratio: 3/4;"
                                    src="{{ $movie->getFirstMediaUrl('ShowImage') }}" alt="">
                                <a href="{{ route('user.movie.show', $movie->id) }}" class="card__play">
                                    <i class="icon ion-ios-play"></i>
                                </a>
                            </div>
                            <div class="card__content">
                                <h3 class="card__title"><a href="#">{{ $movie->show_name }}</a></h3>
                                <span class="card__category">
                                    <a href="#">{{ $movie->category->category_type }}</a>
                                </span>
                                <span class="card__rate"><i
                                        class="icon ion-ios-star"></i>{{ number_format($movie->average_rating, 2) }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- end card -->
                @endforeach

                <!-- paginator -->

                {{ $movies->links('vendor.pagination.custom') }}

                <!-- end paginator -->


            </div>

        </div>
    </div>
    <!-- end catalog -->


</x-guest-layout::guest-layout>
