<x-guest-layout::guest-layout>
    <x-slot name="title">
        Show Movie
    </x-slot>
    <?php header('Access-Control-Allow-Origin: *'); ?>
    <x-slot name="script">
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                const replyButtons = document.querySelectorAll('.reply-button');

                replyButtons.forEach(button => {
                    button.addEventListener('click', function() {
                        const commentId = this.getAttribute('data-comment-id');
                        const replyForm = document.querySelector(
                            `.reply-form[data-comment-id="${commentId}"]`);

                        if (replyForm.style.display === 'none' || replyForm.style.display === '') {
                            replyForm.style.display = 'block';
                        } else {
                            replyForm.style.display = 'none';
                        }
                    });
                });
            });
        </script>

    </x-slot>


    <!-- details -->
    <section class="section details">
        <!-- details background -->
        <div class="details__bg" data-bg={{ asset('guest/assets/img/home/home__bg.jpg') }}></div>
        <!-- end details background -->

        <!-- details content -->
        <div class="container">
            <div class="row">
                <!-- title -->
                <div class="col-12">
                    <h1 class="details__title">{{ $show->show_name }}</h1>
                </div>
                <!-- end title -->

                <!-- content -->
                <div class="col-12 col-xl-6">
                    <div class="card card--details">
                        <div class="row">
                            <!-- card cover -->
                            <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-5">
                                <div class="card__cover">
                                    <img style="background-size: cover;
                                    object-fit: cover;
                                    width: 200px
                                    height: 300px
                                    aspect-ratio: 3/4;
                                    aspect-ratio: 3/4;"
                                        src="{{ $show->getFirstMediaUrl('ShowImage') }}" alt="">
                                </div>
                            </div>
                            <!-- end card cover -->

                            <!-- card content -->
                            <div class="col-12 col-sm-8 col-md-8 col-lg-9 col-xl-7">
                                <div class="card__content">
                                    <div class="card__wrap">
                                        <span class="card__rate"><i
                                                class="icon ion-ios-star"></i>{{ number_format($show->average_rating, 2) }}</span>

                                    </div>

                                    <ul class="card__meta">
                                        <li><span>Genre:</span> <a
                                                href="#">{{ $show->category->category_type }}</a>
                                        <li><span>Release year:</span>
                                            @switch($show->release_date)
                                                @case(App\Enums\ShowYearEnum::YEAR_2020->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2020->getHumanName() }}
                                                @break

                                                @case(App\Enums\ShowYearEnum::YEAR_2021->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2021->getHumanName() }}
                                                @break

                                                @case(App\Enums\ShowYearEnum::YEAR_2022->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2022->getHumanName() }}
                                                @break

                                                @case(App\Enums\ShowYearEnum::YEAR_2023->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2023->getHumanName() }}
                                                @break

                                                @case(App\Enums\ShowYearEnum::YEAR_2024->value)
                                                    {{ App\Enums\ShowYearEnum::YEAR_2024->getHumanName() }}
                                                @break
                                            @endswitch
                                        </li>
                                        <li><span>Price:</span>{{$show->price}}</li>
                                        <li><span>Team Members:</span>
                                            @foreach ($show->teamMembers as $teamMember)
                                                @switch($teamMember->team_member_type)
                                                    @case(App\Enums\TeamMemberTypeEnum::ACTOR->value)
                                                <li><span>{{ $teamMember->team_member_name . ' : ' }}</span>{{ App\Enums\TeamMemberTypeEnum::ACTOR->getHumanName() }}
                                                </li>
                                            @break

                                            @case(App\Enums\TeamMemberTypeEnum::PRODUCER->value)
                                                <li><span>{{ $teamMember->team_member_name . ' : ' }}</span>{{ App\Enums\TeamMemberTypeEnum::ACTOR->getHumanName() }}
                                                </li>
                                            @break

                                            @case(App\Enums\TeamMemberTypeEnum::WRITER->value)
                                                <li><span>{{ $teamMember->team_member_name . ' : ' }}</span>{{ App\Enums\TeamMemberTypeEnum::ACTOR->getHumanName() }}
                                                </li>
                                            @break

                                            @case(App\Enums\TeamMemberTypeEnum::DIRECTOR->value)
                                                <li><span>{{ $teamMember->team_member_name . ' : ' }}</span>{{ App\Enums\TeamMemberTypeEnum::ACTOR->getHumanName() }}
                                                </li>
                                            @break
                                        @endswitch
                                        @endforeach

                                    </ul>

                                    <div class="card__description card__description--details">
                                        {{ $show->summery }}
                                    </div>
                                </div>
                            </div>
                            <!-- end card content -->
                        </div>
                    </div>
                </div>
                <!-- end content -->

                <!-- player -->
                <div class="col-12 col-xl-6">
                    <video controls playsinline
                        poster="../../../cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.jpg" id="player">
                        <!-- Video files -->
                        <source src="{{ $show->getFirstMediaUrl('ShowPromo') }}" type="video/mp4" size="576">
                        <source src="{{ $show->getFirstMediaUrl('ShowPromo') }}" type="video/mp4" size="720">
                        <source src="{{ $show->getFirstMediaUrl('ShowPromo') }}" type="video/mp4" size="1080">
                        <source src="{{ $show->getFirstMediaUrl('ShowPromo') }}" type="video/mp4" size="1440">

                        <!-- Caption files -->
                        <track kind="captions" label="English" srclang="en"
                            src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.en.vtt" default>
                        <track kind="captions" label="Français" srclang="fr"
                            src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.fr.vtt">

                        <!-- Fallback for browsers that don't support the <video> element -->
                        <a href="{{ $show->getFirstMediaUrl('ShowPromo') }}" download>Download</a>
                    </video>
                    @if (auth()->user()->points < $show->points)
                        <button class="form__btn" disabled>Download</button>
                        <div>
                            <span class="card__rate" style="color: rgba(255,255,255,0.75);">You have less than
                                {{ $show->points }} points</span>
                            <br>
                            <span class="card__rate" style="color: rgba(255,255,255,0.75);">Go To </span><a
                                id="pay-button" href="{{ route('user.pay.index') }}"> Pay Page</a>
                        </div>
                    @else
                        <form action="{{ route('user.movie.download', $show) }}" method="GET">
                            <button type="submit" class="form__btn">Download</button>
                        </form>
                    @endif
                    {{-- <form action="{{ route('user.movie.download', $show->getFirstMedia('ShowFile')) }}">
                        <button type="submit" class="form__btn">download</button>
                    </form> --}}
                </div>
                <!-- end player -->




                {{-- <a href="{{ route('user.movie.download',$show->getFirstMedia('ShowFile')) }}">Downlod</a> --}}

                <div class="col-12">
                    <div class="details__wrap">
                        <!-- availables -->
                        <div class="details__devices">

                        </div>
                        <!-- end availables -->

                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="content__head" data-bg={{ asset('guest/assets/img/home/home__bg.jpg') }}>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <!-- content title -->
                            <h2 class="content__title">Discover</h2>
                            <!-- end content title -->

                            <!-- content tabs nav -->
                            <ul class="nav nav-tabs content__tabs" id="content__tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab-1" role="tab"
                                        aria-controls="tab-1" aria-selected="true">Comments</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-2" role="tab"
                                        aria-controls="tab-2" aria-selected="false">Reviews</a>
                                </li>
                            </ul>
                            <!-- end content tabs nav -->

                            <!-- content mobile tabs nav -->
                            <div class="content__mobile-tabs" id="content__mobile-tabs">
                                <div class="content__mobile-tabs-btn dropdown-toggle" role="navigation"
                                    id="mobile-tabs" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <input type="button" value="Comments">
                                    <span></span>
                                </div>

                                <div class="content__mobile-tabs-menu dropdown-menu" aria-labelledby="mobile-tabs">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" id="1-tab"
                                                data-toggle="tab" href="#tab-1" role="tab"
                                                aria-controls="tab-1" aria-selected="true">Comments</a></li>

                                        <li class="nav-item"><a class="nav-link" id="2-tab" data-toggle="tab"
                                                href="#tab-2" role="tab" aria-controls="tab-2"
                                                aria-selected="false">Reviews</a></li>

                                    </ul>
                                </div>
                            </div>
                            <!-- end content mobile tabs nav -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8 col-xl-8">
                        <!-- content tabs -->
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="tab-1" role="tabpanel"
                                aria-labelledby="1-tab">
                                <div class="row">
                                    <!-- comments -->
                                    <div class="col-12">
                                        <div class="comments">
                                            <ul class="comments__list">
                                                @foreach ($comments as $comment)
                                                    <li class="comments__item">
                                                        <div class="comments__autor">
                                                            <img class="comments__avatar"
                                                                src={{ $comment->user->getFirstMediaUrl('User') }}
                                                                alt="">
                                                            <span
                                                                class="comments__name">{{ $comment->user->username }}</span>
                                                            <span
                                                                class="comments__time">{{ $comment->created_at->diffForHumans() }}</span>
                                                        </div>
                                                        <p class="comments__text">{{ $comment->text }}</p>
                                                        <div class="comments__actions">
                                                            <div class="comments__rate">
                                                                <form method="GET"
                                                                    action="{{ route('user.comment.toggleLike', $comment) }}">
                                                                    <button type="submit"><i
                                                                            class="icon ion-md-thumbs-up"></i>{{ $comment->like_count }}</button>
                                                                </form>
                                                            </div>
                                                            <button type="button" class="reply-button"
                                                                data-comment-id="{{ $comment->id }}"><i
                                                                    class="icon ion-ios-share-alt"></i>Reply</button>
                                                        </div>
                                                        <form action={{ route('user.comment.store', $show) }}
                                                            class="form reply-form" method="POST"
                                                            data-comment-id="{{ $comment->id }}"
                                                            style="display: none;">
                                                            @csrf
                                                            <textarea name="text" class="form__textarea" placeholder="Add comment">{{ old('text') }}</textarea>
                                                            @error('text')
                                                                <div style="color: #dc3545;" class="text-danger">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                            <input hidden value={{ $comment->id }} name="parent_id">
                                                            <button type="submit" class="form__btn">Reply</button>
                                                        </form>
                                                    </li>

                                                    @if ($comment->replies->isNotEmpty())
                                                        @foreach ($comment->replies as $reply)
                                                            <li class="comments__item comments__item--answer">

                                                                <div class="comments__autor">
                                                                    <img class="comments__avatar"
                                                                        src={{ $reply->user->getFirstMediaUrl('User') }}
                                                                        alt="">
                                                                    <span
                                                                        class="comments__name">{{ $reply->user->username }}</span>
                                                                    <span
                                                                        class="comments__time">{{ $reply->created_at->diffForHumans() }}</span>
                                                                </div>
                                                                <p class="comments__text">{{ $reply->text }}</p>

                                                            </li>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            </ul>

                                            <form action={{ route('user.comment.store', $show) }} class="form"
                                                method="POST">
                                                @csrf
                                                <textarea name="text" id="text" name="text" class="form__textarea" placeholder="Add comment">{{ old('text') }}</textarea>
                                                @error('text')
                                                    <div style="color: #dc3545;" class="text-danger">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                                <button type="submit" class="form__btn">Send</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- end comments -->
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="2-tab">
                                <div class="row">
                                    <!-- reviews -->
                                    <div class="col-12">
                                        <div class="reviews">
                                            <ul class="reviews__list">
                                                @foreach ($rates as $rate)
                                                    <li class="reviews__item">
                                                        <div class="reviews__autor">
                                                            <img class="reviews__avatar"
                                                                src={{ $rate->user->getFirstMediaUrl('User') }}
                                                                alt="">

                                                            <span
                                                                class="reviews__name">{{ $rate->user->username }}</span>
                                                            <span
                                                                class="reviews__time">{{ $rate->created_at->diffForHumans() }}</span>
                                                            <span class="reviews__rating"><i
                                                                    class="icon ion-ios-star"></i>{{ number_format($rate->value, 2) }}</span>
                                                        </div>

                                                    </li>
                                                @endforeach
                                            </ul>

                                            <form action="{{ route('user.rate.store', $show) }}" method="POST"
                                                class="form">
                                                @csrf
                                                <div class="form__slider">
                                                    <div class="form__slider-rating" id="slider__rating"></div>
                                                    <div class="form__slider-value" id="form__slider-value"></div>
                                                    <input type="hidden" name="value" id="rating_value">

                                                </div>
                                                <button type="submit" class="form__btn">Send</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- end reviews -->
                                </div>
                            </div>
                        </div>
                        <!-- end content tabs -->
                    </div>


                </div>
            </div>
        </section>
        <!-- end details content -->
    </section>
    <!-- end details -->.
</x-guest-layout::guest-layout>
