<x-guest-layout::guest-layout>
    <x-slot name="title">
        Home
    </x-slot>
<!-- home -->
<section class="home">
    <!-- home bg -->
    <div class="owl-carousel home__bg">
        <div class="item home__cover" data-bg={{asset('guest/assets/img/home/home__bg.jpg')}}></div>
        <div class="item home__cover" data-bg={{asset('guest/assets/img/home/home__bg2.jpg')}}></div>
        <div class="item home__cover" data-bg={{asset('guest/assets/img/home/home__bg3.jpg')}}></div>
        <div class="item home__cover" data-bg={{asset('guest/assets/img/home/home__bg4.jpg')}}></div>
    </div>
    <!-- end home bg -->

    <div class="container">
        <div class="row">

            <div class="col-12">
                <h1 class="home__title"><b>Latest</b> MOVIES IN LAST YEAR</h1>

                <button class="home__nav home__nav--prev" type="button">
                    <i class="icon ion-ios-arrow-round-back"></i>
                </button>
                <button class="home__nav home__nav--next" type="button">
                    <i class="icon ion-ios-arrow-round-forward"></i>
                </button>
            </div>

            <div class="col-12">
                <div class="owl-carousel home__carousel">
                    @foreach ($latestMovies as $movie )
                    <div class="item">
                        <!-- card -->
                        <div class="card card--big">
                            <div class="card__cover">
                                <img style="background-size: cover;
                                object-fit: cover;
                                width: 200px
                                height: 300px
                                aspect-ratio: 3/4;
                                aspect-ratio: 3/4;
                               " src={{$movie->getFirstMediaUrl('ShowImage')}} alt="">
                                <a href="{{route('user.movie.show',$movie)}}" class="card__play">
                                    <i class="icon ion-ios-play"></i>
                                </a>
                            </div>
                            <div class="card__content">
                                <h3 class="card__title"><a href="#">{{$movie->show_name}}</a></h3>
                                <span class="card__category">
                                    <a href="">{{$movie->category->category_type}}</a>
                                </span>
                                <span class="card__rate"><i class="icon ion-ios-star"></i>{{number_format($movie->average_rating, 2)}}</span>
                            </div>
                        </div>
                        <!-- end card -->
                    </div>
                    @endforeach



                    {{-- <div class="item">
                        <!-- card -->
                        <div class="card card--big">
                            <div class="card__cover">
                                <img src={{asset('/guest/assets/img/covers/cover2.jpg')}} alt="">
                                <a href="#" class="card__play">
                                    <i class="icon ion-ios-play"></i>
                                </a>
                            </div>
                            <div class="card__content">
                                <h3 class="card__title"><a href="#">Benched</a></h3>
                                <span class="card__category">
                                    <a href="#">Comedy</a>
                                </span>
                                <span class="card__rate"><i class="icon ion-ios-star"></i>7.1</span>
                            </div>
                        </div>
                        <!-- end card -->
                    </div> --}}


                </div>
            </div>
        </div>
    </div>
</section>
<!-- end home -->

<!-- content -->
<section class="content">
    <div class="content__head">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- content title -->
                    <h2 class="content__title">Highest Rates</h2>
                    <!-- end content title -->

                    <!-- content tabs nav -->
                    <ul class="nav nav-tabs content__tabs" id="content__tabs" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="true">MOVIES</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false">TV SERIES</a>
                        </li>


                    </ul>
                    <!-- end content tabs nav -->

                    <!-- content mobile tabs nav -->
                    <div class="content__mobile-tabs" id="content__mobile-tabs">
                        <div class="content__mobile-tabs-btn dropdown-toggle" role="navigation" id="mobile-tabs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <input type="button" value="New items">
                            <span></span>
                        </div>

                        <div class="content__mobile-tabs-menu dropdown-menu" aria-labelledby="mobile-tabs">
                            <ul class="nav nav-tabs" role="tablist">

                                <li class="nav-item"><a class="nav-link" id="1-tab" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="true">MOVIES</a></li>

                                <li class="nav-item"><a class="nav-link" id="2-tab" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false">TV SERIES</a></li>

                            </ul>
                        </div>
                    </div>
                    <!-- end content mobile tabs nav -->
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- content tabs -->
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade  show active" id="tab-2" role="tabpanel" aria-labelledby="2-tab">

                <div class="row">
                    @foreach ($highestRateMovies as $movie )

                    <!-- card -->
                    <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
                        <div class="card">
                            <div class="card__cover">
                                <img style="background-size: cover;
                                object-fit: cover;
                                width: 200px
                                height: 300px
                                aspect-ratio: 3/4;
                                aspect-ratio: 3/4;
                               " src={{$movie->getFirstMediaUrl('ShowImage')}} alt="">
                                <a href="{{route('user.movie.show',$movie)}}" class="card__play">
                                    <i class="icon ion-ios-play"></i>
                                </a>
                            </div>
                            <div class="card__content">
                                <h3 class="card__title"><a href="">{{$movie->show_name}}</a></h3>
                                <span class="card__category">
                                    <a href="">{{$movie->category->category_type}}</a>
                                </span>
                                <span class="card__rate"><i class="icon ion-ios-star"></i>{{number_format($movie->average_rating, 2)}}</span>
                            </div>
                        </div>
                    </div>
                    <!-- end card -->
                    @endforeach

                </div>

            </div>

            <div class="tab-pane fade" id="tab-3" role="tabpanel" aria-labelledby="3-tab">

                <div class="row">
                    @foreach ($highestRateSeries as $series )
                    <!-- card -->
                    <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
                        <div class="card">
                            <div class="card__cover">
                                <img style="background-size: cover;
                                object-fit: cover;
                                width: 200px
                                height: 300px
                                aspect-ratio: 3/4;
                                aspect-ratio: 3/4"  src={{$series->getFirstMediaUrl('ShowImage')}} alt="">
                                <a href="{{route('user.series.show',$series)}}" class="card__play">
                                    <i class="icon ion-ios-play"></i>
                                </a>
                            </div>
                            <div class="card__content">
                                <h3 class="card__title"><a href="#">{{$series->show_name}}</a></h3>
                                <span class="card__category">
                                    <a href="#">{{$series->category->category_type}}</a>
                                </span>
                                <span class="card__rate"><i class="icon ion-ios-star"></i>{{number_format($series->average_rating, 2)}}</span>
                            </div>
                        </div>
                    </div>
                    <!-- end card -->
                    @endforeach

                </div>
            </div>


        </div>
        <!-- end content tabs -->
    </div>
</section>
<!-- end content -->

<!-- expected premiere -->
<section class="section section--bg" data-bg={{asset('guest/assets/img/section/section.jpg')}}>
    <div class="container">
        <div class="row">
            <!-- section title -->
            <div class="col-12">
                <h2 class="section__title">Most TV Series Watched</h2>
            </div>
            <!-- end section title -->
            @foreach ($mostWatched as $movie)

            <!-- card -->
            <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
                <div class="card">
                    <div class="card__cover">
                        <img style=" object-fit: cover;
                                width: 200px
                                height: 300px
                                aspect-ratio: 3/4;
                                aspect-ratio: 3/4;" src="{{$movie->getFirstMediaUrl('ShowImage')}}" alt="">
                        <a href="{{route('user.movie.show',$movie)}}" class="card__play">
                            <i class="icon ion-ios-play"></i>
                        </a>
                    </div>
                    <div class="card__content">
                        <h3 class="card__title"><a href="#">{{$movie->show_name}}</a></h3>
                        <span class="card__category">
                            <a href="">{{$movie->category->category_type}}</a>
                        </span>
                        <span class="card__rate"><i class="icon ion-ios-star"></i>{{number_format($movie->average_rating, 2)}}</span>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- end card -->
        </div>
    </div>
</section>
<!-- end expected premiere -->

<!-- partners -->
<section class="section">
    <div class="container">
        <div class="row">
            <!-- section title -->

            <!-- end section title -->

            <!-- section text -->
            <div class="col-12">

            </div>
            <!-- end section text -->

            <!-- partner -->

            <!-- end partner -->
        </div>
    </div>
</section>
<!-- end partners -->
</x-guest-layout::guest-layout>
