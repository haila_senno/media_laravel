<x-guest-layout::auth-layout>
    <x-slot name="title">
        Log In
    </x-slot>
    <!-- Sign In Start -->
    <form action="{{ route('user.login') }}" method="POST" class="sign__form">
        <div class="logo--area" style="min-width: 250px; max-width: 250px;">
            <h1>
                <a href="" title="">
                    <div class="en-text" style="color:#ff55a5  ;">
                        <span>
                            Just4fun
                        </span>
                    </div>
                </a>
            </h1>
        </div>

        @csrf
        <div class="sign__group">
            <input  name="email" type="email" class="sign__input" placeholder="Email" value="{{old('email')}}">
            @error('email')
            <div style="color: #dc3545;" class="text-danger" >{{$message}}</div>
            @enderror
        </div>

        <div class="sign__group">
            <input  name="password" type="password" class="sign__input" placeholder="Password">
            @error('password')
            <div style="color: #dc3545;" class="text-danger" >{{$message}}</div>
            @enderror
        </div>

        <div class="sign__group sign__group--checkbox">
            <input id="remember" name="remember" type="checkbox" checked="checked">
            <label for="remember">I agree to the <a href="#">Privacy Policy</a></label>
        </div>

        <button type="submit" class="sign__btn">Sign in</button>

        <span class="sign__text">Don't have an Account? <a href="{{route('user.register')}}">Sign Up!</a></span>
    </form>



    <!-- Sign In End -->
</x-guest-layout::auth-layout>
