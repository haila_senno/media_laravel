<x-guest-layout::auth-layout>
    <x-slot name="title">
        Register
    </x-slot>
    <!-- Sign Up Start -->
    <form action="{{ route('user.register') }}" method="POST"  class="sign__form" enctype="multipart/form-data">
        <div class="logo--area" style="min-width: 250px; max-width: 250px;">
            <h1>
                <a href="" title="">
                    <div class="en-text" style="color:#ff55a5  ;">
                        <span>
                            Just4fun
                        </span>
                    </div>
                </a>
            </h1>
        </div>

        @csrf
        <div class="sign__group">
            <input name="username" type="text" class="sign__input" placeholder="User Name" value="{{old('username')}}">
            @error('username')
            <div style="color: #dc3545;" class="text-danger" >{{$message}}</div>
            @enderror
        </div>

        <div class="sign__group">
            <input name="email" type="email" class="sign__input" placeholder="Email" value="{{old('email')}}">
            @error('email')
            <div style="color: #dc3545;" class="text-danger" >{{$message}}</div>
            @enderror

        </div>

        <div class="sign__group">
            <input name="password" type="password" class="sign__input" placeholder="Password">
            @error('password')
            <div style="color: #dc3545;" class="text-danger" >{{$message}}</div>
            @enderror
        </div>

        <div class="sign__group">
            <input name="password_confirmation" type="password" class="sign__input" placeholder="Confirm Password">
            @error('password_confirmation')
            <div style="color: #dc3545;" class="text-danger" >{{$message}}</div>
            @enderror
        </div>
        <div class="sign__group">
            <input name="birth_date" type="date" class="sign__input" placeholder="Birthdate" value="{{old('birth_date')}}">
            @error('birth_date')
            <div style="color: #dc3545;" class="text-danger" >{{$message}}</div>
            @enderror
        </div>

        <div class="sign__group">

            <input  name="image" type="file" class="sign__input" placeholder="Confirm Password">
            @error('image')
            <div style="color: #dc3545;" class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="sign__group sign__group--checkbox">
            <input id="remember" name="remember" type="checkbox" checked="checked">
            <label for="remember">Remember Me</label>
        </div>

        <button class="sign__btn" type="submit">Sign up</button>
        <span class="sign__text">Already have an account? <a href="{{route('user.login')}}">Sign in!</a></span>
    </form>


    <!-- Sign Up End -->
</x-guest-layout::auth-layout>
