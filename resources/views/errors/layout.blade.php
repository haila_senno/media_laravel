<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CUbuntu:300,400,500,700" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href={{ asset('guest/assets/css/bootstrap-reboot.min.css') }}>
    <link rel="stylesheet" href={{ asset('guestassets/css/bootstrap-grid.min.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/owl.carousel.min.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/jquery.mCustomScrollbar.min.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/nouislider.min.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/ionicons.min.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/plyr.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/photoswipe.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/default-skin.css') }}>
    <link rel="stylesheet" href={{ asset('guest/assets/css/main.css') }}>

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="icon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon" href={{ asset('guest/assets/icon/favicon-32x32.png') }}>
    <link rel="apple-touch-icon" sizes="72x72" href={{ asset('guest/assets/icon/apple-touch-icon-72x72.png') }}>
    <link rel="apple-touch-icon" sizes="114x114" href={{ asset('guest/assets/icon/apple-touch-icon-114x114.png') }}>
    <link rel="apple-touch-icon" sizes="144x144" href={{ asset('guest/assets/icon/apple-touch-icon-144x144.png') }}>

    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Dmitry Volkov">
    <title>@yield('title')</title>

    <!-- Styles -->
    {{-- <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
                padding: 20px;
            }
        </style> --}}
</head>

<body>
    <div class="page-404 section--bg" data-bg={{asset('guest/assets/img/section/section.jpg')}}>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="page-404__wrap">
						<div class="page-404__content">
							<h1 class="page-404__title">@yield('message')</h1>
							<p class="page-404__text">The page you are looking for not available!</p>
							<a href="{{route('home')}}" class="page-404__btn">go back to home page</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script src={{ asset('guest/assets/js/jquery-3.3.1.min.js') }}></script>
    <script src={{ asset('guest/assets/js/bootstrap.bundle.min.js') }}></script>
    <script src={{ asset('guest/assets/j.ms/owl.carouselin.js') }}></script>
    <script src={{ asset('guest/assets/js/jquery.mousewheel.min.js') }}></script>
    <script src={{ asset('guest/assets/js/jquery.mCustomScrollbar.min.js') }}></script>
    <script src={{ asset('guest/assets/js/wNumb.js') }}></script>
    <script src={{ asset('guest/assets/js/nouislider.min.js') }}></script>
    <script src={{ asset('guest/assets/js/plyr.min.js') }}></script>
    <script src={{ asset('guest/assets/js/jquery.morelines.min.js') }}></script>
    <script src={{ asset('guest/assets/js/photoswipe.min.js') }}></script>
    <script src={{ asset('guest/assets/js/photoswipe-ui-default.min.js') }}></script>
    <script src={{ asset('guest/assets/js/main.js') }}></script>
</body>


</html>
