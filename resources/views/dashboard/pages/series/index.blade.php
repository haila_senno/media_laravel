<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Series
    </x-slot>

    <!-- Table Start -->
    <div class="container-fluid pt-4 px-4" style="padding: 50px;">
        <div class="row g-4">
            <div class="col-sm-6 col-xl-10">
            <div class="bg-secondary rounded h-100 p-4">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <h6 class="mb-0">TV Series</h6>
                    <form method="GET" action="{{ route('series.index') }}" class="d-none d-md-flex">
                        <input name="filter[show_name]" class="form-control bg-dark border-0 w-70" type="search"
                            placeholder="Search">
                    </form>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Type</th>
                            <th scope="col">Category</th>
                            <th scope="col">Date</th>
                            <th scope="col">Price</th>
                            <th scope="col">Points</th>
                            <th scope="col">Team Members</th>
                            <th scope="col">Image</th>
                            <th scope="col">View Count</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($serieses as $series)
                            {{-- @foreach ($conversations as $conversation) --}}
                            <tr>
                                <th scope="row">{{ $series->id }}</th>
                                <td scope="row">{{ $series->show_name }}</td>
                                <td scope="row">{{ App\Enums\ShowTypeEnum::SERIES->getHumanName() }} </td>
                                <td scope="row">{{ $series->category->category_type }}</td>
                                <td scope="row">
                                    @switch($series->release_date)
                                        @case(App\Enums\ShowYearEnum::YEAR_2020->value)
                                            {{ App\Enums\ShowYearEnum::YEAR_2020->getHumanName() }}
                                        @break

                                        @case(App\Enums\ShowYearEnum::YEAR_2021->value)
                                            {{ App\Enums\ShowYearEnum::YEAR_2021->getHumanName() }}
                                        @break

                                        @case(App\Enums\ShowYearEnum::YEAR_2022->value)
                                            {{ App\Enums\ShowYearEnum::YEAR_2022->getHumanName() }}
                                        @break

                                        @case(App\Enums\ShowYearEnum::YEAR_2023->value)
                                            {{ App\Enums\ShowYearEnum::YEAR_2023->getHumanName() }}
                                        @break

                                        @case(App\Enums\ShowYearEnum::YEAR_2024->value)
                                            {{ App\Enums\ShowYearEnum::YEAR_2024->getHumanName() }}
                                        @break
                                    @endswitch
                                </td>
                                <td scope="row">{{ $series->price }}</td>
                                <td scope="row">{{ $series->points }}</td>
                                <td scope="row">
                                    @foreach ($series->teamMembers as $teamMember)
                                        {{ $teamMember->team_member_name . '|' }}
                                    @endforeach
                                </td>
                                <td><img style="background-size: cover;
                                object-fit: cover;
                                width: 100px;
                                aspect-ratio: 3/2;
                                aspect-ratio: 3/2;"
                                        src="{{ $series->getFirstMediaUrl('ShowImage') }}" alt=""></td>
                                <td scope="row">{{ $series->view_count }}</td>
                                <td scope="row">
                                    <a href="{{ route('series.edit', $series) }}"><button type="button"
                                            class="btn btn-warning m-2" style="padding-right: 20px;">Edit</button></a>

                                    <button class="btn btn-danger" type="button" data-bs-toggle="modal"
                                        data-original-title="test" data-bs-target="#deleteModal_{{ $series->id }}">
                                        Delete
                                    </button>
                                    <div class="modal fade" id="deleteModal_{{ $series->id }}" tabindex="-1"
                                        role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 style="color: #212F3D " class="modal-title"
                                                        id="deleteModalLabel">Delete
                                                        Series</h5>
                                                    <button class="btn-close" type="button" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <div style="color: #212F3D " class="modal-body">Are You sure to delete
                                                    {{ $series->show_name }} series ?
                                                </div>

                                                <div class="modal-footer">
                                                    <form style="display:initial"
                                                        action={{ route('series.destroy', $series) }} method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-primary" type="button"
                                                            data-bs-dismiss="modal">Close
                                                        </button>
                                                        <button class="btn btn-danger" type="submit">Delete
                                                        </button>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>

                            </tr>
                        @endforeach
                        {{-- @endforeach --}}

                    </tbody>
                </table>

            </div>



        </div>
        <a href="{{ route('series.create') }}"><button type="button" class="btn btn-success m-2"
                style="padding-right: 30px;">Add</button></a>
            </div>
    </div>
    <!-- Table End -->

</x-dashboard-layout::dashboard-layout>
