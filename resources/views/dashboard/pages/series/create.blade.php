<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
    Create Series
    </x-slot>
    <!-- Form Start -->
 <div class="container-fluid pt-4 px-4">
    <div class="row g-4">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <form action="{{ route('series.store') }}" method="POST" enctype="multipart/form-data">
          @csrf

        <div class="col-sm-12 col-xl-6">
            <div class="bg-secondary rounded h-100 p-4">
                <h6 class="mb-4">Add new series</h6>

                <select class="form-select form-select-sm mb-3" required="" aria-label="s.form-select-sm example" name="category_id">
                    <option selected disabled value="">Classifiaction by type
                    </option>
                    @foreach ($categories as $category)
                        <option @selected(old('category_id')  == $category->id)  value={{ $category->id }}>{{ $category->category_type }}</option>
                    @endforeach
                </select>

                <select name="release_date" class="form-select form-select-sm mb-3" aria-label=".form-select-sm example">
                    <option selected>Select the year</option>
                    @foreach ($years as $key => $year)
                    <option @selected(old('release_date') == $key)  value={{ $key }}>{{$year}}
                    </option>
                    @endforeach

                </select>

                <div class="mb-3">
                    <label for="price" class="form-label">The price</label>
                 <input name="price" type="number" class="form-control" id="release_date" value="{{ old('price') }}">
                </div>




                    <div class="mb-3">
                        <label for="show_name" class="form-label">The name</label>
                        <input value="{{old('show_name')}}" name="show_name" class="form-control bg-dark" type="text" id="show_name">
                    </div>
                    <div class="mb-3">
                        <label for="summery" class="form-label">The summary</label>
                        <input value="{{old('summery')}}" name="summery" class="form-control bg-dark" type="text" id="summery">
                    </div>

                    <div class="mb-3">
                        <label for="formFile" class="form-label">Team members</label>
                        <select class="form-select js-example-placeholder-multiple col-sm-12" multiple="multiple"
                                name="team_members[]" required="">
                            @foreach ($teamMembers as $teamMember)
                                <option @selected(!!Arr::first(old('team_members', []), fn($el) => $el == $teamMember->id)) value="{{ $teamMember->id }}">
                                    @switch($teamMember->team_member_type)
                                    @case(App\Enums\TeamMemberTypeEnum::ACTOR->value)
                                        {{ $teamMember->team_member_name . ' : ' . App\Enums\TeamMemberTypeEnum::ACTOR->getHumanName() }}
                                    @break

                                    @case(App\Enums\TeamMemberTypeEnum::PRODUCER->value)
                                        {{ $teamMember->team_member_name . ' : ' . App\Enums\TeamMemberTypeEnum::PRODUCER->getHumanName() }}
                                    @break

                                    @case(App\Enums\TeamMemberTypeEnum::WRITER->value)
                                        {{ $teamMember->team_member_name . ' : ' . App\Enums\TeamMemberTypeEnum::WRITER->getHumanName() }}
                                    @break

                                    @case(App\Enums\TeamMemberTypeEnum::DIRECTOR->value)
                                        {{ $teamMember->team_member_name . ' : ' . App\Enums\TeamMemberTypeEnum::DIRECTOR->getHumanName() }}
                                    @break
                                @endswitch
                                </option>
                            @endforeach
                        </select>
                    </div>


            </div>
        </div>
        <div class="col-sm-12 col-xl-6">
            <div class="bg-secondary rounded h-100 p-4">
                <h6 class="mb-4">File Input</h6>
                <div class="mb-3">
                    <label for="formFile" class="form-label">promo file</label>
                    <input name="promo" class="form-control bg-dark" type="file" id="formFile">
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Default image</label>
                    <input name="image" class="form-control bg-dark" type="file" id="formFile">
                </div>
                <button type="submit" class="btn btn-outline-primary m-2">Add</button>
            </div>
        </div>
        </div>

        </form>

</div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>

