<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Edit Series
    </x-slot>
    <!-- Form Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

        <form action="{{ route('series.update',$show->id) }}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')

        <div class="col-sm-12 col-xl-6">
            <div class="bg-secondary rounded h-100 p-4">
                <h6 class="mb-4">Edit  {{$show->show_name}} Series</h6>

                <select class="form-select form-select-sm mb-3" required="" aria-label="s.form-select-sm example" name="category_id">
                    <option selected disabled value="">Classifiaction by type
                    </option>
                    @foreach ($categories as $category)
                    <option @selected($category->id == $show->category_id) value={{ $category->id }}>
                        {{ $category->category_type }}</option>
                   @endforeach
                </select>

                <div class="mb-3">
                    <label for="formFile" class="form-label">the Price</label>
                    <input name="price" type="number" class="form-control" id="release_date" value={{$show->price}}>
                </div>

                <select name="release_date" class="form-select form-select-sm mb-3" aria-label=".form-select-sm example" aria-valuenow="{{ $show->release_date }}">
                    <option selected>Select the year</option>
                    @foreach ($years as $key => $year)
                    <option @selected($key == $show->release_date) value={{ $key }}>{{$year}}
                    </option>
                    @endforeach
                </select>



                    <div class="mb-3">
                        <label for="show_name" class="form-label">the name</label>
                        <input name="show_name" class="form-control bg-dark" type="text" id="show_name" value="{{ $show->show_name }}">
                    </div>
                    <div class="mb-3">
                        <label for="summery" class="form-label">the summary</label>
                        <input name="summery" class="form-control bg-dark" type="text" id="summery" value="{{ $show->summery }}">
                    </div>

                    <div class="mb-3">
                        <label for="formFile" class="form-label">Team members</label>
                        <select class="form-select js-example-placeholder-multiple col-sm-12" multiple="multiple"
                                name="team_members[]" >
                            @foreach ($teamMembers as $teamMember)
                            <option @selected(!!Arr::first(old('teamMembers') ?? $show->teamMembers()->allRelatedIds(), fn($el) => $el == $teamMember->id)) value={{ $teamMember->id }}>
                                @switch($teamMember->team_member_type)
                                @case(App\Enums\TeamMemberTypeEnum::ACTOR->value)
                                    {{ $teamMember->team_member_name . ' : ' . App\Enums\TeamMemberTypeEnum::ACTOR->getHumanName() }}
                                @break

                                @case(App\Enums\TeamMemberTypeEnum::PRODUCER->value)
                                    {{ $teamMember->team_member_name . ' : ' . App\Enums\TeamMemberTypeEnum::PRODUCER->getHumanName() }}
                                @break

                                @case(App\Enums\TeamMemberTypeEnum::WRITER->value)
                                    {{ $teamMember->team_member_name . ' : ' . App\Enums\TeamMemberTypeEnum::WRITER->getHumanName() }}
                                @break

                                @case(App\Enums\TeamMemberTypeEnum::DIRECTOR->value)
                                    {{ $teamMember->team_member_name . ' : ' . App\Enums\TeamMemberTypeEnum::DIRECTOR->getHumanName() }}
                                @break
                            @endswitch
                          </option>
                            @endforeach
                        </select>
                    </div>


            </div>
        </div>
        <div class="col-sm-12 col-xl-6">
            <div class="bg-secondary rounded h-100 p-4">
                <h6 class="mb-4">File Input</h6>
                <div class="mb-3">
                    <label for="promo" class="form-label">promo file</label>
                    <input name="promo" class="form-control bg-dark" type="file" id="promo" >
                </div>
                <div class="mb-3">
                    <label for="image" class="form-label">Default image</label>
                    <input name="image" class="form-control bg-dark" type="file" id="image" >
                </div>
                <a href=""><button type="submit" class="btn btn-outline-primary m-2">Edit</button></a>
            </div>

        </div>

        </form>

</div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>
