<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Dashboard
    </x-slot>
    <x-slot name="script">
        <script>
            var ctx = document.getElementById('worldwide-sales').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: @json($chartData['labels']),
                    datasets: [{
                        label: 'Show Counts',
                        data: @json($chartData['shows']),
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        borderWidth: 1
                    }, ]
                },
                options: {
                    scales: {
                        responsive: true

                    }
                }
            });
            var ctx2 = document.getElementById('salse-revenue').getContext('2d');
            var myChart2 = new Chart(ctx2, {
                type: "line",
                data: {
                    labels: @json($chartData['labels']),
                    datasets: [{
                        label: "View Counts",
                        data: @json($chartData['views']),
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        fill: true
                    }, ]
                },
                options: {
                    responsive: true
                }
            });
            var ctx3 = document.getElementById('rate-chart').getContext('2d');
            var myChart3 = new Chart(ctx3, {
                type: "bar",
                data: {
                    labels: @json($chartData['labels']),
                    datasets: [{
                        label: "Rate Average",
                        data: @json($chartData['rates']),
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        fill: true
                    }, ]
                },
                options: {
                    responsive: true
                }
            });
            var ctx4 = document.getElementById('pay-chart').getContext('2d');
            var myChart4 = new Chart(ctx4, {
                type: "line",
                data: {
                    labels: @json($chartData['labels']),
                    datasets: [{
                        label: "Shows Count",
                        data: @json($chartData['payed']),
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        fill: true
                    }, ]
                },
                options: {
                    responsive: true
                }
            });
        </script>
    </x-slot>
    <!-- Sale & Revenue Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-6 col-xl-3">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-chart-line fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Number of Movies</p>
                        <h6 class="mb-0">{{ $movies }}</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-chart-bar fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Number of Series</p>
                        <h6 class="mb-0">{{ $series }}</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-chart-area fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Number of Games</p>
                        <h6 class="mb-0">{{ $games }}</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-chart-pie fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Number of Subscribers</p>
                        <h6 class="mb-0">{{ $users }}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sale & Revenue End -->


    <!-- Sales Chart Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Shows Order by counts</h6>
                    </div>
                    <canvas id="worldwide-sales"></canvas>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Shows Order By View Counts</h6>
                    </div>
                    <canvas id="salse-revenue"></canvas>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Shows Order By Highest Rates</h6>
                    </div>
                    <canvas id="rate-chart"></canvas>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Most Shows Downloaded</h6>
                    </div>
                    <canvas id="pay-chart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- Sales Chart End -->


    <!-- Recent Sales Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary text-center rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Most Games Downloaded</h6>
            </div>
            <div class="table-responsive">
                <table class="table text-start align-middle table-bordered table-hover mb-0">
                    <thead>
                        <tr class="text-white">
                            <th scope="col">#</th>
                            <th scope="col">Game Name</th>
                            <th scope="col">Downloaded Count</th>
                            <th scope="col">Price</th>
                            <th scope="col">Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($paidGames as $game)
                                <tr>
                                    <td>{{ $game->id }}</td>
                                    <td>{{ $game->game_name }}</td>
                                    <td>{{ $game->users_count }}</td>
                                    <td>{{ $game->price }}</td>
                                    <td>{{ $game->created_at->diffForHumans() }}</td>
                                </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Recent Sales End -->
</x-dashboard-layout::dashboard-layout>
