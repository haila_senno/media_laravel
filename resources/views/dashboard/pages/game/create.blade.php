<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Create Game
    </x-slot>
    <!-- Form Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('game.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Add new game</h6>

                    <select name="date" class="form-select form-select-sm mb-3" aria-label=".form-select-sm example">
                        <option selected>Select the year</option>
                        @foreach ($years as $key => $year)
                        <option @selected(old('date') == $key) value={{ $key }}>{{$year}}
                        </option>
                        @endforeach

                    </select>

                    <select name="game_type" class="form-select form-select-sm mb-3" aria-label=".form-select-sm example">
                        <option selected>is the game free?</option>
                        @foreach ($game_types as $key => $game_type)
                        <option @selected(old('game_type') == $key)  value={{ $key }}>{{$game_type}}
                        </option>
                    @endforeach
                    </select>


                    <div class="mb-3">
                        <label for="game_name" class="form-label">the name</label>
                        <input value="{{old('game_name')}}" name="game_name" class="form-control bg-dark" type="text" id="game_name">
                    </div>

                    <div class="mb-3">
                        <label for="price" class="form-label">The price</label>
                        <input name="price" type="number" class="form-control" id="price"
                            value="{{ old('price') }}">
                    </div>

                    <div class="mb-3">
                        <label for="summery" class="form-label">the summary</label>
                        <input value="{{old('summery')}}"  name="summery" class="form-control bg-dark" type="text" id="summery">
                    </div>


                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Game Input</h6>
                    <div class="mb-3">
                        <label for="setup" class="form-label">SetUp File</label>
                        <input name="setup" class="form-control bg-dark" type="file" id="setup">
                    </div>
                    <div class="mb-3">
                        <label for="image1" class="form-label">Image1</label>
                        <input name="image1" class="form-control bg-dark" type="file" id="image1">
                    </div>
                    <div class="mb-3">
                        <label for="image2" class="form-label">Image2</label>
                        <input name="image2" class="form-control bg-dark" type="file" id="image2">
                    </div>
                    <div class="mb-3">
                        <label for="image3" class="form-label">Image3</label>
                        <input name="image3" class="form-control bg-dark" type="file" id="image3">
                    </div>
                    <a href=""><button type="submit" class="btn btn-outline-primary m-2">Add</button></a>

                </div>
            </div>
            </form>

    </div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>

