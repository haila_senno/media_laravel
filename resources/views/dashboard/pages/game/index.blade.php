<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Game
    </x-slot>

    <!-- Table Start -->
    <div class="container-fluid pt-4 px-4" style="padding: 50px;">
        <div class="row g-4">
            <div class="col-sm-12 col-xl-8">
                <div class="bg-secondary rounded h-100 p-4">
                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <h6 class="mb-0">Games</h6>
                        <form method="GET" action="{{route('game.index')}}" class="d-none d-md-flex">
                            <input name="filter[game_name]" class="form-control bg-dark border-0 w-70" type="search" placeholder="Search">
                        </form>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Type</th>
                                <th scope="col">Date</th>
                                <th scope="col">image</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($games as $game)
                                <tr>
                                    <th scope="row">{{ $game->id }}</th>
                                    <td>{{ $game->game_name }}</td>
                                    <td>
                                        @switch($game->game_type)
                                            @case(App\Enums\GameTypeEnum::FREE->value)
                                                {{ App\Enums\GameTypeEnum::FREE->getHumanName() }}
                                            @break

                                            @case(App\Enums\GameTypeEnum::NOT_FREE->value)
                                                {{ App\Enums\GameTypeEnum::NOT_FREE->getHumanName() }}
                                            @break
                                        @endswitch
                                    </td>
                                    <td> @switch($game->date)
                                            @case(App\Enums\ShowYearEnum::YEAR_2020->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2020->getHumanName() }}
                                            @break

                                            @case(App\Enums\ShowYearEnum::YEAR_2021->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2021->getHumanName() }}
                                            @break

                                            @case(App\Enums\ShowYearEnum::YEAR_2022->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2022->getHumanName() }}
                                            @break

                                            @case(App\Enums\ShowYearEnum::YEAR_2023->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2023->getHumanName() }}
                                            @break

                                            @case(App\Enums\ShowYearEnum::YEAR_2024->value)
                                                {{ App\Enums\ShowYearEnum::YEAR_2024->getHumanName() }}
                                            @break
                                        @endswitch
                                    </td>
                                    <td><img style="background-size: cover;
                                object-fit: cover;
                                width: 100px;
                                aspect-ratio: 3/2;
                                aspect-ratio: 3/2;"
                                            src="{{ $game->getFirstMediaUrl('Game1') }}" alt=""></td>
                                    <td scope="row">
                                        <a href="{{ route('game.edit', $game) }}"><button type="button"
                                                class="btn btn-warning m-2"
                                                style="padding-right: 20px;">Edit</button></a>

                                        <button class="btn btn-danger" type="button" data-bs-toggle="modal"
                                            data-original-title="test"
                                            data-bs-target="#deleteModal_{{ $game->id }}">
                                            Delete
                                        </button>
                                        <div class="modal fade" id="deleteModal_{{ $game->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 style="color: #212F3D" class="modal-title" id="deleteModalLabel">Delete
                                                            Game</h5>
                                                        <button class="btn-close" type="button" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div style="color: #212F3D " class="modal-body">Are You sure to delete {{$game->game_name}} game ?
                                                    </div>

                                                    <div class="modal-footer">
                                                        <form style="display:initial"
                                                            action={{ route('game.destroy', $game) }} method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-primary" type="button"
                                                                data-bs-dismiss="modal">Close
                                                            </button>
                                                            <button class="btn btn-danger" type="submit">Delete
                                                            </button>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach



                        </tbody>
                    </table>

                </div>

            </div>

        </div>
        <a href="{{ route('game.create') }}"><button type="button" class="btn btn-success m-2"
                style="padding-right: 30px;">Add</button></a>

    </div>
    <!-- Table End -->

</x-dashboard-layout::dashboard-layout>
