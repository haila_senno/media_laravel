<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Edit Admin
    </x-slot>
    <!-- Form Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('admin.update',$admin->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Edit  {{$admin->username}} Admin</h6>

                    <div class="mb-3">
                        <label for="username" class="form-label">Admin Name</label>
                        <input type="text" name="username" class="form-control" id="username" value="{{$admin->username}}">
                    </div>

                    <div class="mb-3">
                        <label for="email" class="form-label">Admin Email</label>
                        <input type="email" name="email" class="form-control" id="email" value="{{$admin->email}}">
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="password">
                    </div>

                    <div class="mb-3">
                        <label for="password_confirmation" class="form-label">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                    </div>

                    <div class="mb-3">
                        <label for="image" class="form-label">Image</label>
                        <input type="file" name="image" class="form-control" id="image" >
                    </div>



                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                    <a href=""><button type="submit" class="btn btn-outline-primary m-2">Edit</button></a>
                </div>
            </div>
            </form>

    </div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>
