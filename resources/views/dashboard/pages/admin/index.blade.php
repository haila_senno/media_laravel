<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Admin
    </x-slot>

    <!-- Table Start -->
    <div class="container-fluid pt-4 px-4" style="padding: 50px;">
        <div class="row g-4">
            <div class="col-sm-12 col-xl-8">
                <div class="bg-secondary rounded h-100 p-4">
                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <h6 class="mb-0">Admins</h6>
                        <form method="GET" action="{{route('admin.index')}}" class="d-none d-md-flex">
                            <input name="filter[username]" class="form-control bg-dark border-0 w-70" type="search" placeholder="Search">
                        </form>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Admin Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Image</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($admins as $admin)
                                <tr>
                                    <th scope="row">{{ $admin->id }}</th>
                                    <td>{{ $admin->username }}</td>
                                    <td> {{ $admin->email }} </td>
                                    <td><img style="background-size: cover;
                                        object-fit: cover;
                                        width: 100px;
                                        aspect-ratio: 3/2;
                                        aspect-ratio: 3/2;"
                                                    src="{{ $admin->getFirstMediaUrl('Admin') }}" alt=""></td>
                                    <td scope="row">
                                        <a href="{{ route('admin.edit', $admin) }}"><button type="button"
                                                class="btn btn-warning m-2"
                                                style="padding-right: 20px;">Edit</button></a>

                                        <button class="btn btn-danger" type="button" data-bs-toggle="modal"
                                            data-original-title="test"
                                            data-bs-target="#deleteModal_{{ $admin->id }}">
                                            Delete
                                        </button>
                                        <div class="modal fade" id="deleteModal_{{ $admin->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 style="color: #212F3D " class="modal-title"
                                                            id="deleteModalLabel">Delete
                                                            Admin</h5>
                                                        <button class="btn-close" type="button" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div style="color: #212F3D " class="modal-body">Are You sure to
                                                        delete {{ $admin->username }} admin ?
                                                    </div>

                                                    <div class="modal-footer">
                                                        <form style="display:initial"
                                                            action={{ route('admin.destroy', $admin) }}
                                                            method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-primary" type="button"
                                                                data-bs-dismiss="modal">Close
                                                            </button>
                                                            <button class="btn btn-danger" type="submit">Delete
                                                            </button>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach



                        </tbody>
                    </table>

                </div>

            </div>

        </div>
        <a href="{{ route('admin.create') }}"><button type="button" class="btn btn-success m-2"
                style="padding-right: 30px;">Add</button></a>

    </div>
    <!-- Table End -->

</x-dashboard-layout::dashboard-layout>
