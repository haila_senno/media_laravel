<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Edit Setting
    </x-slot>
    <!-- Form Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('setting.update',$setting->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Edit  {{$setting->id}} Setting</h6>

                    <div class="mb-3">
                        <label for="points" class="form-label">Points</label>
                        <input type="number" name="points" class="form-control" id="points" value="{{ $setting->points }}">
                    </div>

                    <div class="mb-3">
                        <label for="price" class="form-label">Price</label>
                        <input type="number" name="price" class="form-control" id="price" value="{{ $setting->price }}">
                    </div>
                    </select>



                </div>
            </div>
            <div class="col-sm-12 col-xl-6">

                    <a href=""><button type="submit" class="btn btn-outline-primary m-2">Edit</button></a>

                </div>
            </div>
            </form>

    </div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>
