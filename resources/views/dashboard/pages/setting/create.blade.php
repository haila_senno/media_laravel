<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Create Setting
    </x-slot>
    <!-- Form Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('setting.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Add new setting</h6>

                    <div class="mb-3">
                        <label for="points" class="form-label">Points</label>
                        <input type="number" name="points" class="form-control" id="points" value="{{ old('points') }}">
                    </div>

                    <div class="mb-3">
                        <label for="price" class="form-label">Price</label>
                        <input type="number" name="price" class="form-control" id="price" value="{{ old('price') }}">
                    </div>

                </div>
            </div>
            <div class="col-sm-12 col-xl-6">

                    <a href=""><button type="submit" class="btn btn-outline-primary m-2">Add</button></a>

                </div>
            </div>
            </form>

    </div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>

