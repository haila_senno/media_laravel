<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
         Points
    </x-slot>

<!-- Table Start -->
<div class="container-fluid pt-4 px-4" style="padding: 50px;">
    <div class="row g-4">
        <div class="col-sm-12 col-xl-6">
            <div class="bg-secondary rounded h-100 p-4">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <h6 class="mb-0">Points</h6>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Points</th>
                            <th scope="col">Price</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($settings as $setting)
                        <tr>
                            <th scope="row">{{ $setting->id }}</th>
                            <td>{{ $setting->points }}</td>
                            <td>{{ $setting->price }}</td>
                            <td scope="row">
                                <a href="{{ route('setting.edit',$setting) }}"><button  type="button" class="btn btn-warning m-2" style="padding-right: 20px;" >Edit</button></a>

                                <button class="btn btn-danger" type="button" data-bs-toggle="modal"
                                data-original-title="test"
                                data-bs-target="#deleteModal_{{ $setting->id }}">
                                Delete
                            </button>
                            <div class="modal fade" id="deleteModal_{{ $setting->id }}"
                                tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 style="color: #212F3D " class="modal-title" id="deleteModalLabel">Delete
                                                Setting</h5>
                                            <button class="btn-close" type="button"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div style="color: #212F3D " class="modal-body">Are You sure to delete setting with points {{$setting->points}} ?
                                        </div>

                                        <div class="modal-footer">
                                            <form style="display:initial"
                                                action={{ route('setting.destroy', $setting) }}
                                                method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-primary" type="button"
                                                    data-bs-dismiss="modal">Close
                                                </button>
                                                <button class="btn btn-danger"
                                                    type="submit">Delete
                                                </button>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </div>
    @if($settings->isEmpty())
    <a href="{{route('setting.create')}}"><button  type="button" class="btn btn-success m-2" style="padding-right: 30px;">Add</button></a>
    @endIf

</div>
<!-- Table End -->

</x-dashboard-layout::dashboard-layout>



