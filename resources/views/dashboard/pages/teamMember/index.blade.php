<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
         Team Member
    </x-slot>

<!-- Table Start -->
<div class="container-fluid pt-4 px-4" style="padding: 50px;">
    <div class="row g-4">
        <div class="col-sm-12 col-xl-6">
            <div class="bg-secondary rounded h-100 p-4">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <h6 class="mb-0">Team Members</h6>
                    <form method="GET" action="{{route('teamMember.index')}}" class="d-none d-md-flex">
                        <input name="filter[team_member_name]" class="form-control bg-dark border-0 w-70" type="search" placeholder="Search">
                    </form>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Type</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($teamMembers as $teamMember)
                        <tr>
                            <th scope="row">{{ $teamMember->id }}</th>
                            <td>{{ $teamMember->team_member_name }}</td>
                            <td>
                                @switch($teamMember->team_member_type)
                                @case(App\Enums\TeamMemberTypeEnum::ACTOR->value)
                                    {{App\Enums\TeamMemberTypeEnum::ACTOR->getHumanName() }}
                                @break

                                @case(App\Enums\TeamMemberTypeEnum::PRODUCER->value)
                                    {{App\Enums\TeamMemberTypeEnum::PRODUCER->getHumanName() }}
                                @break

                                @case(App\Enums\TeamMemberTypeEnum::WRITER->value)
                                    {{App\Enums\TeamMemberTypeEnum::WRITER->getHumanName() }}
                                @break

                                @case(App\Enums\TeamMemberTypeEnum::DIRECTOR->value)
                                    {{App\Enums\TeamMemberTypeEnum::DIRECTOR->getHumanName() }}
                                @break
                            @endswitch
                            </td>
                            <td scope="row">
                                <a href="{{ route('teamMember.edit',$teamMember) }}"><button  type="button" class="btn btn-warning m-2" style="padding-right: 20px;" >Edit</button></a>

                                <button class="btn btn-danger" type="button" data-bs-toggle="modal"
                                data-original-title="test"
                                data-bs-target="#deleteModal_{{ $teamMember->id }}">
                                Delete
                            </button>
                            <div class="modal fade" id="deleteModal_{{ $teamMember->id }}"
                                tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 style="color: #212F3D " class="modal-title" id="deleteModalLabel">Delete
                                                Team Member</h5>
                                            <button class="btn-close" type="button"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div style="color: #212F3D " class="modal-body">Are You sure to delete {{$teamMember->team_member_name}} team member ?
                                        </div>

                                        <div class="modal-footer">
                                            <form style="display:initial"
                                                action={{ route('teamMember.destroy', $teamMember) }}
                                                method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-primary" type="button"
                                                    data-bs-dismiss="modal">Close
                                                </button>
                                                <button class="btn btn-danger"
                                                    type="submit">Delete
                                                </button>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            </td>

                        </tr>
                        @endforeach



                    </tbody>
                </table>

            </div>

        </div>

    </div>
    <a href="{{route('teamMember.create')}}"><button  type="button" class="btn btn-success m-2" style="padding-right: 30px;">Add</button></a>

</div>
<!-- Table End -->

</x-dashboard-layout::dashboard-layout>



