<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Edit Team Member
    </x-slot>
    <!-- Form Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('teamMember.update',$teamMember->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Edit  {{$teamMember->team_member_name}} Team Member</h6>

                    <div class="mb-3">
                        <label for="team_member_name" class="form-label">Team Member Name</label>
                        <input type="text" name="team_member_name" class="form-control" id="team_member_name" value="{{ $teamMember->team_member_name }}">
                    </div>

                    <select name="team_member_type" class="form-select form-select-sm mb-3" aria-label=".form-select-sm example">
                        <option disabled selected>Select the team member type</option>
                        @foreach ($teamMemberTypes as $key => $type)
                        <option @selected($key == $teamMember->team_member_type)  value={{ $key }}>{{$type}}
                        </option>
                    @endforeach
                    </select>



                </div>
            </div>
            <div class="col-sm-12 col-xl-6">

                    <a href=""><button type="submit" class="btn btn-outline-primary m-2">Edit</button></a>

                </div>
            </div>
            </form>

    </div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>
