<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Create Episode
    </x-slot>
    <!-- Form Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('episode.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Add new episode</h6>

                    <div class="mb-3">
                        <label for="formFile" class="form-label">Episode Name</label>
                        <input value="{{old('episode_name')}}" type="text" name="episode_name" class="form-control" id="release_date" value="{{ old('episode_name') }}">
                    </div>

                    <select name="show_id" class="form-select form-select-sm mb-3" aria-label=".form-select-sm example">
                        <option disabled selected>Select the series name</option>
                        @foreach ($shows as $show)
                        <option @selected(old('show_id') == $show->id)  value={{ $show->id }}>{{$show->show_name}}
                        </option>
                    @endforeach
                    </select>

                    <div class="mb-3">
                        <label for="episode_number" class="form-label">Episode Number</label>
                        <input value="{{old('episode_number')}}"  name="episode_number" class="form-control bg-dark" type="number" id="episode_number" value={{old('episode_number')}}>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Episode Input</h6>
                    <div class="mb-3">
                        <label for="formFile" class="form-label">Episode file</label>
                        <input name="file" class="form-control bg-dark" type="file" id="formFile">
                    </div>

                    <a href=""><button type="submit" class="btn btn-outline-primary m-2">Add</button></a>

                </div>
            </div>
            </form>

    </div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>

