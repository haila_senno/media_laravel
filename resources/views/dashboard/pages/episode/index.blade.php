<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Episode
    </x-slot>

<!-- Table Start -->
<div class="container-fluid pt-4 px-4" style="padding: 50px;">
    <div class="row g-4">
        <div class="col-sm-12 col-xl-6">
            <div class="bg-secondary rounded h-100 p-4">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <h6 class="mb-0">Episodes</h6>
                    <form method="GET" action="{{route('episode.index')}}" class="d-none d-md-flex">
                        <input name="filter[episode_name]" class="form-control bg-dark border-0 w-70" type="search" placeholder="Search">
                    </form>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Episode Number</th>
                            <th scope="col">Series Name</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($episodes as $episode)
                        <tr>
                            <th scope="row">{{ $episode->id }}</th>
                            <td>{{ $episode->episode_name }}</td>
                            <td>{{ $episode->episode_number }}</td>
                            <td>{{ $episode->show->show_name }}</td>
                            <td scope="row">
                                <a href="{{ route('episode.edit',$episode) }}"><button  type="button" class="btn btn-warning m-2" style="padding-right: 20px;" >Edit</button></a>

                                <button class="btn btn-danger" type="button" data-bs-toggle="modal"
                                data-original-title="test"
                                data-bs-target="#deleteModal_{{ $episode->id }}">
                                Delete
                            </button>
                            <div class="modal fade" id="deleteModal_{{ $episode->id }}"
                                tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 style="color: #212F3D " class="modal-title" id="deleteModalLabel">Delete
                                                Episode</h5>
                                            <button class="btn-close" type="button"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div style="color: #212F3D " class="modal-body">Are You sure to delete {{$episode->episode_name}} episode ?
                                        </div>

                                        <div class="modal-footer">
                                            <form style="display:initial"
                                                action={{ route('episode.destroy', $episode) }}
                                                method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-primary" type="button"
                                                    data-bs-dismiss="modal">Close
                                                </button>
                                                <button class="btn btn-danger"
                                                    type="submit">Delete
                                                </button>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            </td>

                        </tr>
                        @endforeach



                    </tbody>
                </table>

            </div>

        </div>

    </div>
    <a href="{{route('episode.create')}}"><button  type="button" class="btn btn-success m-2" style="padding-right: 30px;">Add</button></a>

</div>
<!-- Table End -->

</x-dashboard-layout::dashboard-layout>



