<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Pay
    </x-slot>

    <!-- Table Start -->
    <div class="d-flex justify-content-center align-items-center vh-100 ">
        <div class="col-sm-12 col-xl-5 owl-carousel testimonial-carousel">
            <div class="bg-secondary rounded h-100 p-4">
                <h6 class="mb-4">Buy Points</h6>
                <hr>
                <div class="row ">
                    <div class="col-md-6">
                <a class="mb-4">User informations:</a>
                <div class="price__item--first"><span>Created At:</span>
                    <span>{{ $user->created_at->diffForHumans() }}</span></div>
                <div class="price__item--first"><span>User Email:</span> <span>{{ $user->email }}</span></div>
                <br>
                <br>
                <a class="mb-4">User Order:</a>
                <div class="price__item--first"><span>Points : </span> <span>{{ $setting->points }}</span></div>
                <div class="price__item--first"><span>Price : </span> <span>{{ $setting->price }}$</span></div>
            </div>
                <div class="col-md-6 d-flex flex-column align-items-center ">
                    <div class="testimonial-item text-center">
                        <img class="img-fluid rounded-circle mx-auto mb-4" src={{ $user->getFirstMediaUrl('User') }}
                            style="width: 100px; height: 100px;">
                        <h5 class="mb-1">{{ $user->username }}</h5>
                        <p>Total Points : {{ $user->points }}</p>
                        <div id="pay-actions">
                            <form method="POST" action="{{ route('pay.accept', [$user, $setting]) }}">
                                @csrf
                                <button type="submit" class="btn btn-outline-success m-2">Accept</button>
                            </form>
                            <form method="POST" action="{{ route('pay.reject', [$user, $setting]) }}">
                                @csrf
                                <button type="submit" class="btn btn-outline-danger m-2">Reject</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!-- Table End -->

</x-dashboard-layout::dashboard-layout>
