<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Edit Category
    </x-slot>
    <!-- Form Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('category.update',$category->id) }}" method="POST" >
            @csrf
            @method('PUT')

            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Edit  {{$category->category_type}} Category</h6>

                    <div class="mb-3">
                        <label for="category_type" class="form-label">Category Type</label>
                        <input type="text" name="category_type" class="form-control" id="category_type" value="{{$category->category_type}}">
                    </div>

                </div>
            </div>
            <div class="col-sm-12 col-xl-6">

                    <a href=""><button type="submit" class="btn btn-outline-primary m-2">Edit</button></a>

                </div>
            </div>
            </form>

    </div>
<!-- Form End -->
</x-dashboard-layout::dashboard-layout>
