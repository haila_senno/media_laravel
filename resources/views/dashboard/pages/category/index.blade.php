<x-dashboard-layout::dashboard-layout>
    <x-slot name="title">
        Category
    </x-slot>

    <!-- Table Start -->
    <div class="container-fluid pt-4 px-4" style="padding: 50px;">
        <div class="row g-4">
            <div class="col-sm-12 col-xl-8">
                <div class="bg-secondary rounded h-100 p-4">
                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <h6 class="mb-0">Categories</h6>
                        <form method="GET" action="{{route('category.index')}}" class="d-none d-md-flex">
                            <input name="filter[category_type]" class="form-control bg-dark border-0 w-70" type="search" placeholder="Search">
                        </form>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Category Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                                <tr>
                                    <th scope="row">{{ $category->id }}</th>
                                    <td>{{ $category->category_type }}</td>


                                    <td scope="row">
                                        <a href="{{ route('category.edit', $category) }}"><button type="button"
                                                class="btn btn-warning m-2"
                                                style="padding-right: 20px;">Edit</button></a>

                                        <button class="btn btn-danger" type="button" data-bs-toggle="modal"
                                            data-original-title="test"
                                            data-bs-target="#deleteModal_{{ $category->id }}">
                                            Delete
                                        </button>
                                        <div class="modal fade" id="deleteModal_{{ $category->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 style="color: #212F3D " class="modal-title"
                                                            id="deleteModalLabel">Delete
                                                            Category</h5>
                                                        <button class="btn-close" type="button" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div style="color: #212F3D " class="modal-body">Are You sure to
                                                        delete {{ $category->category_type }} category ?
                                                    </div>

                                                    <div class="modal-footer">
                                                        <form style="display:initial"
                                                            action={{ route('category.destroy', $category) }}
                                                            method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-primary" type="button"
                                                                data-bs-dismiss="modal">Close
                                                            </button>
                                                            <button class="btn btn-danger" type="submit">Delete
                                                            </button>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach



                        </tbody>
                    </table>

                </div>

            </div>

        </div>
        <a href="{{ route('category.create') }}"><button type="button" class="btn btn-success m-2"
                style="padding-right: 30px;">Add</button></a>

    </div>
    <!-- Table End -->

</x-dashboard-layout::dashboard-layout>
