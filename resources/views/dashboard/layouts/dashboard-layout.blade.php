<x-dashboard-layout::app-layout :title="$title">
    <x-slot name="head">
        {{ $head ?? '' }}
    </x-slot>
    <x-slot name="script">
        {{ $script ?? '' }}
    </x-slot>

    <!-- Sidebar Start -->
    <div class="sidebar pe-4 pb-3">
        <nav class="navbar bg-secondary navbar-dark">

            <div class="d-flex align-items-center ms-4 mb-4">
                <div class="logo--area" style="min-width: 250px; max-width: 250px;">
                    <h1>
                        <a href="" title="">
                            <div class="en-text" style="color:#ff55a5  ;">
                                <span>
                                    Just4fun
                                </span>
                            </div>
                        </a>
                    </h1>
                </div>

            </div>
            <div class="navbar-nav w-100">
                <a href="{{route('dashboard')}}" class="nav-item nav-link active"><i
                        class="fa fa-tachometer-alt me-2"></i>Dashboard</a>

                <a href="{{route('movie.index')}}" class="nav-item nav-link"><i class="fa fa-th me-2"></i>Movies</a>
                <a href="{{route('series.index')}}" class="nav-item nav-link"><i class="fa fa-th me-2"></i>TV</a>
                <a href="{{route('game.index')}}" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Games</a>
                <a href="{{route('episode.index')}}" class="nav-item nav-link"><i class="fa fa-table me-2"></i>Episodes</a>
                <a href="{{route('teamMember.index')}}" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Team
                    Members</a>
                <a href="{{route('admin.index')}}" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Admins</a>
                <a href="{{route('user.index')}}" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Users</a>
                <a href="{{route('category.index')}}" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Categories</a>
                <a href="{{route('setting.index')}}" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Points</a>

            </div>
        </nav>
    </div>
    <!-- Sidebar End -->

    <!-- Content Start -->
    <div class="content" style="background-image: url('{{asset('dashboard/assets/img/home__bg.jpg')}}'); position: absolute; ">
        <!-- Navbar Start -->
        <nav class="navbar navbar-expand bg-secondary navbar-dark sticky-top px-4 py-0">
            <a href="index.html" class="navbar-brand d-flex d-lg-none me-4">
                <h2 class="text-primary mb-0"><i class="fa fa-user-edit"></i></h2>
            </a>
            <a href="#" class="sidebar-toggler flex-shrink-0">
                <i class="fa fa-bars"></i>
            </a>
            {{-- <form class="d-none d-md-flex ms-4">
                <input class="form-control bg-dark border-0" type="search" placeholder="Search">
            </form> --}}
            <div class="navbar-nav align-items-center ms-auto">

                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                        <img class="rounded-circle me-lg-2" src={{auth()->user()->getFirstMediaUrl('Admin')}} alt="" style="width: 40px; height: 40px;">
                        <span class="d-none d-lg-inline-flex">{{auth()->user()->username}}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end bg-secondary border-0 rounded-0 rounded-bottom m-0">
                        {{-- <a href="#" class="dropdown-item">My Profile</a> --}}
                        {{-- <a href="#" class="dropdown-item">Settings</a> --}}
                        <a href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log Out</a>
                        <a href="{{ route('home') }}" class="dropdown-item" >Home</a>

                        <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>
                </div>
            </div>
        </nav>
        <!-- Navbar End -->


        {{ $slot }}

        <!-- Footer Start -->
        <div class="container-fluid pt-4 px-4">
            <div class="bg-secondary rounded-top p-4">
                <div class="row">
                    <div class="col-12 col-sm-6 text-center text-sm-start">
                        &copy; <a href="#">Our Site Name Just4fun.</a>
                    </div>

                </div>
            </div>
        </div>
        <!-- Footer End -->
    </div>
    <!-- Content End -->
    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
</x-dashboard-layout::app-layout>
