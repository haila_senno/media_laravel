<?php

use App\Enums\RoleEnum;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Guest\Game\GameController;
use App\Http\Controllers\Guest\Game\OnlineGameController;
use App\Http\Controllers\Guest\HomeController;
use App\Http\Controllers\Guest\Show\MovieController;
use App\Http\Controllers\Guest\Show\SeriesController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group([
    'middleware' => ['auth','checkRole:' . RoleEnum::ADMIN->value],
], function () {

    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

});

 Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::group([
    "prefix" => 'user',
], function () {
    require __DIR__ . '/user/main.php';
});

require __DIR__ . '/auth.php';

Route::group([
    "prefix" => 'user',
    "middleware" => ['auth','checkRole:' . RoleEnum::ADMIN->value . '|' . RoleEnum::USER->value]
], function () {
    require __DIR__ . '/user/user.php';
});
Route::group([
    "prefix" => 'admin',
    "middleware" => ['auth','checkRole:' . RoleEnum::ADMIN->value]
], function () {
    require __DIR__ . '/admin/admin.php';
});







