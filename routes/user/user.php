<?php

use App\Enums\RoleEnum;
use App\Http\Controllers\Dashboard\EpisodeController;
use App\Http\Controllers\Guest\Game\GameController;
use App\Http\Controllers\Guest\PayController;
use App\Http\Controllers\Guest\CommentController;
use App\Http\Controllers\Guest\Game\OnlineGameController;
use App\Http\Controllers\Guest\HomeController;
use App\Http\Controllers\guest\LikeController;
use App\Http\Controllers\Guest\RateController;
use App\Http\Controllers\Guest\Show\MovieController;
use App\Http\Controllers\Guest\Show\SeriesController;
use App\Http\Controllers\guest\UserController;
use App\Http\Controllers\Guest\PlayController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group([
    'prefix'=>'movie',
    'as'=>'user.movie.'
],function() {
    Route::get('/{show}', [MovieController::class, 'show'])->name('show');
    Route::get('/media/download/{show}', [MovieController::class, 'download'])->name('download');
});

Route::group([
    'prefix'=>'series',
    'as'=>'user.series.'
],function() {
    Route::get('/{show}', [SeriesController::class, 'show'])->name('show');
    Route::get('{show}/media/download', [SeriesController::class, 'download'])->name('download');

});

Route::group([
    'prefix'=>'game',
    'as'=>'user.game.'
],function() {
    Route::get('/{game}', [GameController::class, 'show'])->name('show');
    Route::get('{game}/media/download', [GameController::class, 'download'])->name('download');
    // Route::post('/{game}/play', [PlayController::class, 'play'])->name('play');

});

Route::group([
    'prefix'=>'onlineGame',
    'as'=>'user.game.online.'
],function() {
    Route::get('/userPoints', [OnlineGameController::class, 'getUserPoints'])->name('userPoints');
    Route::get('/{game}', [OnlineGameController::class, 'show'])->name('show');
    Route::post('/{game}/play', [OnlineGameController::class, 'play'])->name('play');

});


Route::group([
    'prefix'=>'comment',
    'as'=>'user.comment.'
],function() {
    Route::post('/show/{show}', [CommentController::class, 'store'])->name('store');

});

Route::group([
    'prefix'=>'rate',
    'as'=>'user.rate.'
],function() {
    Route::post('/show/{show}', [RateController::class, 'store'])->name('store');

});

Route::group([
    'prefix'=>'like',
    'as'=>'user.comment.'
],function() {
    Route::get('comment/{comment}', [LikeController::class, 'toggleLike'])->name('toggleLike');

});

Route::group([
    'prefix'=>'pay',
    'as'=>'user.pay.'
],function() {
    Route::get('/', [PayController::class, 'index'])->name('index');
    Route::post('/{setting}', [PayController::class, 'buy'])->name('buy');

});

Route::group([
    'prefix'=>'user',
    'as'=>'user.'
],function() {
    Route::get('/', [UserController::class, 'show'])->name('show');
    Route::put('/{user}', [UserController::class, 'update'])->name('update');

});









