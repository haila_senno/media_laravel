<?php

use App\Enums\RoleEnum;
use App\Http\Controllers\Dashboard\EpisodeController;
use App\Http\Controllers\Guest\Game\GameController;
use App\Http\Controllers\Guest\PayController;
use App\Http\Controllers\Guest\CommentController;
use App\Http\Controllers\Guest\Game\OnlineGameController;
use App\Http\Controllers\Guest\HomeController;
use App\Http\Controllers\guest\LikeController;
use App\Http\Controllers\Guest\RateController;
use App\Http\Controllers\Guest\Show\MovieController;
use App\Http\Controllers\Guest\Show\SeriesController;
use App\Http\Controllers\guest\UserController;
use App\Http\Controllers\Guest\PlayController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group([
    'prefix' => 'movie',
    'as' => 'user.movie.',

], function () {
    Route::get('/', [MovieController::class, 'index'])->name('index');
});

Route::group([
    'prefix' => 'series',
    'as' => 'user.series.',

], function () {
    Route::get('/', [SeriesController::class, 'index'])->name('index');
});

Route::group([
    'prefix' => 'game',
    'as' => 'user.game.',

], function () {
    Route::get('/', [GameController::class, 'index'])->name('index');
});

Route::group([
    'prefix' => 'onlineGame',
    'as' => 'user.game.online.',

], function () {
    Route::get('/', [OnlineGameController::class, 'index'])->name('index');
});


Route::get('/about', [HomeController::class, 'about'])->name('about');

Route::get('/help', [HomeController::class, 'help'])->name('help');
