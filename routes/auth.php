<?php

use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\Dashboard\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Dashboard\Auth\RegisteredUserController;
use App\Http\Controllers\Guest\Auth\AuthenticatedSessionController as UserAuthenticatedSessionController;
use App\Http\Controllers\Guest\Auth\RegisteredUserController as UserRegisteredUserController;
use Illuminate\Support\Facades\Route;

// Route::group([
//     'middleware' => 'guest',
//     'prefix' => 'auth/admin',
//     'as' => 'admin.',
// ], function () {
//     Route::get('register', [RegisteredUserController::class, 'create'])
//         ->name('register');

//     Route::post('register', [RegisteredUserController::class, 'store'])
//         ->name('register');


//     Route::get('login', [AuthenticatedSessionController::class, 'create'])
//         ->name('login');

//     Route::post('login', [AuthenticatedSessionController::class, 'store'])
//         ->name('login');
// });
// // Route::middleware('guest')->group(function () {
// // });

// Route::group([
//     'middleware' => 'auth',
//     'prefix' => 'auth/admin',
//     'as' => 'admin.',
// ], function () {
//     Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
//         ->name('logout');
// });


Route::group([
    'middleware' => 'guest',
    'prefix' => 'auth/user',
    'as' => 'user.',
], function () {
    Route::get('register', [UserRegisteredUserController::class, 'create'])
        ->name('register');

    Route::post('register', [UserRegisteredUserController::class, 'store'])
        ->name('register');

    Route::get('login', [UserAuthenticatedSessionController::class, 'create'])
        ->name('login');

    Route::post('login', [UserAuthenticatedSessionController::class, 'store'])
        ->name('login');
});
// Route::middleware('guest')->group(function () {
// });

Route::group([
    'middleware' => 'auth',
    'prefix' => 'auth/user',
    'as' => 'user.',
], function () {
    Route::post('logout', [UserAuthenticatedSessionController::class, 'destroy'])
        ->name('logout');
});
