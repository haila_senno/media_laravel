<?php

use App\Enums\RoleEnum;
use App\Http\Controllers\Dashboard\AdminController;
use App\Http\Controllers\Dashboard\CategoryController;
use App\Http\Controllers\Dashboard\EpisodeController;
use App\Http\Controllers\Dashboard\GameController;
use App\Http\Controllers\Dashboard\PayController;
use App\Http\Controllers\Dashboard\SettingController;
use App\Http\Controllers\Dashboard\Show\MovieController;
use App\Http\Controllers\Dashboard\Show\SeriesController;
use App\Http\Controllers\Dashboard\ShowController;
use App\Http\Controllers\Dashboard\TeamMemberController;
use App\Http\Controllers\Dashboard\UserController;
use App\Http\Controllers\Guest\HomeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::group([
    'prefix'=>'movie',
    'as'=>'movie.'
],function() {
    Route::get('/create', [MovieController::class, 'create'])->name('create');
    Route::get('/', [MovieController::class, 'index'])->name('index');
    Route::delete('/{show}', [MovieController::class, 'destroy'])->name('destroy');
    Route::put('/{show}', [MovieController::class, 'update'])->name('update');
    Route::get('/{show}', [MovieController::class, 'edit'])->name('edit');
    Route::post('/', [MovieController::class, 'store'])->name('store');
});

Route::group([
    'prefix'=>'series',
    'as'=>'series.'
],function() {
    Route::get('/create', [SeriesController::class, 'create'])->name('create');
    Route::get('/', [SeriesController::class, 'index'])->name('index');
    Route::delete('/{show}', [SeriesController::class, 'destroy'])->name('destroy');
    Route::put('/{show}', [SeriesController::class, 'update'])->name('update');
    Route::get('/{show}', [SeriesController::class, 'edit'])->name('edit');
    Route::post('/', [SeriesController::class, 'store'])->name('store');
});

Route::group([
    'prefix'=>'game',
    'as'=>'game.'
],function() {
    Route::get('/create', [GameController::class, 'create'])->name('create');
    Route::get('/', [GameController::class, 'index'])->name('index');
    Route::delete('/{game}', [GameController::class, 'destroy'])->name('destroy');
    Route::put('/{game}', [GameController::class, 'update'])->name('update');
    Route::get('/{game}', [GameController::class, 'edit'])->name('edit');
    Route::post('/', [GameController::class, 'store'])->name('store');
});

Route::group([
    'prefix'=>'episode',
    'as'=>'episode.'
],function() {
    Route::get('/create', [EpisodeController::class, 'create'])->name('create');
    Route::get('/', [EpisodeController::class, 'index'])->name('index');
    Route::delete('/{episode}', [EpisodeController::class, 'destroy'])->name('destroy');
    Route::put('/{episode}', [EpisodeController::class, 'update'])->name('update');
    Route::get('/{episode}', [EpisodeController::class, 'edit'])->name('edit');
    Route::post('/', [EpisodeController::class, 'store'])->name('store');
});

Route::group([
    'prefix'=>'teamMember',
    'as'=>'teamMember.'
],function() {
    Route::get('/create', [TeamMemberController::class, 'create'])->name('create');
    Route::get('/', [TeamMemberController::class, 'index'])->name('index');
    Route::delete('/{teamMember}', [TeamMemberController::class, 'destroy'])->name('destroy');
    Route::put('/{teamMember}', [TeamMemberController::class, 'update'])->name('update');
    Route::get('/{teamMember}', [TeamMemberController::class, 'edit'])->name('edit');
    Route::post('/', [TeamMemberController::class, 'store'])->name('store');
});

Route::group([
    'prefix'=>'admin',
    'as'=>'admin.'
],function() {
    Route::get('/create', [AdminController::class, 'create'])->name('create');
    Route::get('/', [AdminController::class, 'index'])->name('index');
    Route::delete('/{admin}', [AdminController::class, 'destroy'])->name('destroy');
    Route::put('/{admin}', [AdminController::class, 'update'])->name('update');
    Route::get('/{admin}', [AdminController::class, 'edit'])->name('edit');
    Route::post('/', [AdminController::class, 'store'])->name('store');
});

Route::group([
    'prefix'=>'user',
    'as'=>'user.'
],function() {
    Route::get('/create', [UserController::class, 'create'])->name('create');
    Route::get('/', [UserController::class, 'index'])->name('index');
    Route::delete('/{user}', [UserController::class, 'destroy'])->name('destroy');
    Route::put('/{user}', [UserController::class, 'update'])->name('updateUser');
    Route::get('/{user}', [UserController::class, 'edit'])->name('edit');
    Route::post('/', [UserController::class, 'store'])->name('store');
});

Route::group([
    'prefix'=>'pay',
    'as'=>'pay.'
],function() {
    Route::get('/user/{user}/setting/{setting}', [PayController::class, 'index'])->name('index');
    Route::post('/user/{user}/setting/{setting}/accept', [PayController::class, 'accept'])->name('accept');
    Route::post('/user/{user}/setting/{setting}/reject', [PayController::class, 'reject'])->name('reject');
});

Route::group([
    'prefix'=>'setting',
    'as'=>'setting.'
],function() {
    Route::get('/create', [SettingController::class, 'create'])->name('create');
    Route::get('/', [SettingController::class, 'index'])->name('index');
    Route::delete('/{setting}', [SettingController::class, 'destroy'])->name('destroy');
    Route::put('/{setting}', [SettingController::class, 'update'])->name('update');
    Route::get('/{setting}', [SettingController::class, 'edit'])->name('edit');
    Route::post('/', [SettingController::class, 'store'])->name('store');
});

Route::group([
    'prefix'=>'category',
    'as'=>'category.'
],function() {
    Route::get('/create', [CategoryController::class, 'create'])->name('create');
    Route::get('/', [CategoryController::class, 'index'])->name('index');
    Route::delete('/{category}', [CategoryController::class, 'destroy'])->name('destroy');
    Route::put('/{category}', [CategoryController::class, 'update'])->name('update');
    Route::get('/{category}', [CategoryController::class, 'edit'])->name('edit');
    Route::post('/', [CategoryController::class, 'store'])->name('store');
});






