<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up(): void
    {
        Schema::create('shows', function (Blueprint $table) {
            $table->id();
            $table->string('show_name');
            $table->unsignedInteger('show_type');
            $table->integer('price');
            $table->text('summery');
            $table->unsignedBigInteger('release_date');
            $table->unsignedBigInteger('view_count')->default(0);
            $table->foreignIdFor(Category::class)->constrained('categories')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('shows');
    }
};
