<?php

use App\Models\Show;
use App\Models\TeamMember;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up(): void
    {
        Schema::create('show_team_member', function (Blueprint $table) {
            $table->id();
            ######## Foreign keys  ########

            $table->foreignIdFor(Show::class)->constrained('shows')->cascadeOnDelete();
            $table->foreignIdFor(TeamMember::class)->constrained('team_members')->cascadeOnDelete();

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('show_team_member');
    }
};
