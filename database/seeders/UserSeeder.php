<?php

namespace Database\Seeders;

use App\Enums\RoleEnum;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{


    public function run(): void
    {
        User::firstOrCreate([
            'email' => "admin@hotmail.com",
            'username' => "admin",
            'password' => Hash::make('12345678'),
            ])->assignRole(RoleEnum::ADMIN->value);
        User::firstOrCreate([
            'email' => "user@hotmail.com",
            'username'=>"user",
            'password' => Hash::make('12345678'),
            'birth_date'=>"2000-05-30"
        ])->assignRole(RoleEnum::USER->value);

        // User::factory(30)->create()
        // ->each(fn($user) => $user->assignRole(RoleEnum::USER->value));
    }
}
