<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{


    public function run(): void
    {
        Category::create([
            'category_type' =>'Asian',

        ]);
        Category::create([
            'category_type' =>'Horror',

        ]);
        Category::create([

            'category_type' =>'Romance',

        ]);
        Category::create([

            'category_type' =>'Animation',
        ]);
        Category::create([

            'category_type' =>'Action',
        ]);
    }
}
