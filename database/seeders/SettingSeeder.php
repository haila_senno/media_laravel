<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{


    public function run(): void
    {
        Setting::create([
            'price'=>1000,
            'points'=>100
         ]);

         Setting::create([
            'price'=>2000,
            'points'=>200
         ]);
         Setting::create([
            'price'=>3000,
            'points'=>300
         ]);
         Setting::create([
            'price'=>4000,
            'points'=>400
         ]);
         Setting::create([
            'price'=>5000,
            'points'=>500
         ]);
         Setting::create([
            'price'=>6000,
            'points'=>600
         ]);
         Setting::create([
            'price'=>7000,
            'points'=>700
         ]);
    }
}
